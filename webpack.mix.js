let mix = require('laravel-mix');
const tailwindcss = require('tailwindcss'); 
//require('laravel-mix-tailwind');
//require('laravel-mix-polyfill');
//require('laravel-mix-purgecss');



// mix
//     .js(['src/static/js/prism.js','src/static/js/app.js'], 'static/js/app.js')
// 	.tailwind()
//     .sass('src/static/scss/app.scss', 'static/css/app.css')
//     .sass('src/static/scss/preview.scss', 'static/css/preview.css')
//     .sourceMaps()
//     .setPublicPath('_site')
//     .version();


mix.js('src/assets/js/app.js', 'assets/js/app.js')
    .sass('src/assets/scss/app.scss', 'assets/css/app.css')
    .sass('src/assets/scss/preview.scss', 'assets/css/preview.css')
    .options({
        postCss: [ tailwindcss('./tailwind.config.js') ],
    })
	.sourceMaps()
    .setPublicPath('_site')
    .version();