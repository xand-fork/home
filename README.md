# Transparent Developer Portal

### License

MIT OR Apache-2.0

### Technologies used:

- [Netlify CMS](https://www.netlifycms.org/)
- [Eleventy](https://www.11ty.dev/)
- [Alpine.js](https://github.com/alpinejs/alpine)
- [Tailwind CSS](https://tailwindcss.com/)

### 1. Clone this Repository

```
git clone git@gitlab.com:TransparentIncDevelopment/product/website/home.git
```

### 2. Navigate to the directory

```
cd home
```

### 3. Install dependencies

```
npm install
```

### 4. Build the project to generate the first CSS

This step is only required the very first time.

```
npm run build
```

### 5. Run Eleventy

```
npx netlify-cms-proxy-server
npm run start
```

