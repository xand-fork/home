module.exports = {
	//mode: 'jit',
	content: [
		'./src/**/*.scss',
		'./src/**/*.js',
		'./src/**/*.njk',
		'./src/*.njk',
		'./src/**/*.html',
		'./src/*.html'
	],
	theme: {
		backgroundPosition: {
			bottom: 'bottom',
			center: 'center',
			left: 'left',
			'left-bottom': 'left bottom',
			'left-top': 'left top',
			right: 'right',
			'right-bottom': 'right bottom',
			'right-top': 'right top',
			'right-btn': 'right 1rem center',
			top: 'top',
		},
		fontFamily: {
			'sans': ['"Telegraf"', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
			'body': ['"IBMPlexSans"', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
		},
		extend: {
			backgroundPostion: {
				'btnbg': 'right 1rem center',
			},
			borderWidth: {
				DEFAULT: '1px',
				'1': '1px',
				'2': '2px',
				'3': '3px',
				'4': '4px',
				'6': '6px',
				'10': '10px',
				'16': '16px',
				'32': '32px',
			},
			colors: {
				white: '#F3F4F4',
				brown: {
					DEFAULT: '#4B401F',
					'light': '#938C79',
					'lighter': '#E4E2DD',
					'lightest': '#F6F5F4',
				},
				tan: {
					DEFAULT: '#968041',
					'light': '#C0B38D',
					'lighter': '#EFECE2',
					'lightest': '#FAF9F5',
				},
				slate: {
					DEFAULT: '#0C2026',
					'light': '#6D797D',
					'lighter': '#DBDEDE',
					'lightest': '#F3F4F4',
					'dark': '#081F26',
				},
				salmon: {
					DEFAULT: '#EC4625',
					'light': '#F4907C',
					'lighter': '#FCE3DE',
					'lightest': '#FEF6F4'
				},
				miami: '#5A5AE5',
				vice: '#55F1A6',
				ltgray: '#E5EAED',
			},
			cursor: {
				crosshair: 'crosshair',
				pointer: 'pointer',
				'ns-resize': 'ns-resize',
				'zoom-in': 'zoom-in',
			},
			height: {
				'sm': '150px',
				'md': '300px',
				'lg': '450px',
				'xl': '600px',
				'2xl': '750px',
				'3xl': '900px',
				'4xl': '1800px',
			},
			inset: {
				'0': 0,
				'auto': 'auto',
				'1/2': '50%'
			},
			lineHeight: {
				'snuggy': '1.42'
			},
			minHeight: {
				'sm': '150px',
				'md': '300px',
				'lg': '450px',
				'xl': '600px',
				'2xl': '750px',
				'3xl': '900px',
				'4xl': '1800px',
			},
			opacity: {
				'80': '0.8',
				'85': '0.85',
				'90': '0.9',
				'95': '0.95',
			},
			rotate: {
				'135': '135deg',
				'225': '225deg',
				'315': '315deg',
			},
			transitionDelay: {
				'1250': '1250ms',
				'1500': '1500ms',
				'2000': '2000ms',
			},
			transitionDuration: {
				'1500': '1500ms',
				'2000': '2000ms',
			},
			transitionTimingFunction: {
				'in-expo': 'cubic-bezier(0.95, 0.05, 0.795, 0.035)',
				'out-expo': 'cubic-bezier(0.19, 1, 0.22, 1)',
				'slowjerk': 'cubic-bezier(0.8, 0, 0.2, 1)',
				'in-back': 'cubic-bezier(0.600, -0.280, 0.735, 0.045)'
			},
		}
	},
	variants: {
		extend: {
			opacity: ['disabled'],
			cursor: ['disabled'],
		},
	},
	corePlugins: {
		// container: false
	},
	plugins: [
		require('@tailwindcss/aspect-ratio'),
		function ({ addComponents }) {
			addComponents({
				'.container': {
					width: '100%',
					'@screen sm': {
						maxWidth: '640px',
					},
					'@screen md': {
						maxWidth: '768px',
					},
					'@screen lg': {
						maxWidth: '1024px',
					},
					'@screen xl': {
						maxWidth: '1200px',
					},
				}
			})
		}
	],
	future: {
		removeDeprecatedGapUtilities: true,
		purgeLayersByDefault: true,
	},
}
