const yaml = require("js-yaml");
const { DateTime } = require("luxon");
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const { EleventyRenderPlugin } = require("@11ty/eleventy");
const htmlmin = require("html-minifier");
const inspect = require("util").inspect;
const markdownIt = require('markdown-it')({
  html: true,
  breaks: true,
  linkify: true
});
const markdownItAttrs = require('markdown-it-attrs');
const markdownLib = markdownIt.use(markdownItAttrs)
//SVGs
// const svgContents = require("eleventy-plugin-svg-contents");

module.exports = function (eleventyConfig) {
  // Disable automatic use of your .gitignore
  eleventyConfig.setUseGitIgnore(false);

  // Merge data instead of overriding
  eleventyConfig.setDataDeepMerge(true);

  // human readable date
  eleventyConfig.addFilter("readableDate", (dateStr) => {
    //DateTime.fromISO(dateStr);
    return DateTime.fromJSDate(dateStr).toFormat(
      "LLL dd, yyyy"
    );
  });

  eleventyConfig.addFilter("replace", function (value, src, rep) {
    var reg = new RegExp(src, 'gi');
    return value.replace(reg, rep);
  });

  eleventyConfig.addNunjucksFilter("limit", (arr, limit) => arr.slice(0, limit));

  eleventyConfig.addFilter("debug", (content) => `<pre>${inspect(content)}</pre>`);

  // for trimming off /s and whatnot from the template
  eleventyConfig.addFilter("trim_end", function (value, hair) {
    if (value.charAt(value.length - 1) == hair) {
      value = value.slice(0, -1);
    }
    return value;
    // return value.replace(/hair+$/, "");
  });

  // eleventyConfig.addFilter("pluck", function (arr, value, attr) {
  //   return arr.filter((item) => item.data[attr] === value);
  // });

  eleventyConfig.addFilter("isLive", function (value) {
    return value.filter(item => item.data.status === 'live');
  });

  // eleventyConfig.addFilter("filterByTitle", function (value, title) {
  //   return value.filter(item => item.data.title === title );
  // });

  eleventyConfig.addFilter("filterBySlug", function (value, slug) {
    return value.filter(item => item.fileSlug === slug);
  });

  // eleventyConfig.addFilter("filterById", function (value, id) {
  //   return value.filter(item => item.id === id );
  // });

  // eleventyConfig.addFilter("nameById", function (value, id) {
  //   return value.filter(item => item.id === id )
  // 	  		      .map(item => item.name);
  // });

  // eleventyConfig.addFilter("imageById", function (value, id) {
  // return value.filter(item => item.id === id )
  // 			      .map(item => item.image);
  // });

  // eleventyConfig.addFilter("imageBySlug", function (value, slug) {
  //   return value.filter(item => item.fileSlug === slug )
  //               .map(item => item.image);
  // });

  //add markdown attributes
  eleventyConfig.setLibrary('md', markdownLib);
  eleventyConfig.addFilter('markdown', value => {
    return `${markdownIt.render(value)}`
  });

  //SVGs
  // eleventyConfig.addPlugin(svgContents);

  // navigation plugin added
  eleventyConfig.addPlugin(eleventyNavigationPlugin);

  //render templates in other templates plugin
  eleventyConfig.addPlugin(EleventyRenderPlugin);


  // Syntax Highlighting for Code blocks
  // eleventyConfig.addPlugin(syntaxHighlight);

  // To Support .yaml Extension in _data
  // You may remove this if you can use JSON
  eleventyConfig.addDataExtension("yaml", (contents) =>
    yaml.safeLoad(contents)
  );


  eleventyConfig.addCollection("livePosts", function (collectionApi) {
    // get unsorted items
    return collectionApi.getFilteredByTag('post').filter(item => item.data.status == 'live');
  });

  eleventyConfig.addCollection("livePressAndBlog", function (collectionApi) {
    // get unsorted items
    return collectionApi.getFilteredByTag('pressAndBlog').filter(item => item.data.status == 'live');
  });

  // Create collection from _data/customData.js
  // eleventyConfig.addCollection("apiCollection", (collection) => {
  //   const allItems = collection.getAll()[0].data.api;

  //   // Filter or use another method to select the items you want
  //   // for the collection
  //   return allItems.filter((item) => {
  //     // ...
  //   });
  // });

  // eleventyConfig.addCollection('articles', async collection => {
  //   const response = await getEndpoint({ url: host + '/api/articles' })
  //   return response.data
  // })

  // build API collections
  // eleventyConfig.addCollection("membersAPI", function (collection) {
  //   return collection.getFilteredByGlob("api/swagger/*.md");
  // });

  // Copy Static Files to /_Site
  eleventyConfig.addPassthroughCopy({
    "./src/admin/config.yml": "./admin/config.yml",
    "./node_modules/alpinejs/dist/cdn.min.js": "./assets/js/alpine.js",
    // "./node_modules/prismjs/themes/prism-tomorrow.css": "./assets/css/prism-tomorrow.css",
  });

  // Copy Image Folder to /_site
  eleventyConfig.addPassthroughCopy("./src/assets/images");

  // Copy Documents Folder to /_site
  eleventyConfig.addPassthroughCopy("./src/assets/documents");

  // Copy Xandbox Folder to /_site
  eleventyConfig.addPassthroughCopy("./src/assets/xandbox");

  // Copy fonts folder to /_site
  eleventyConfig.addPassthroughCopy("./src/assets/fonts");

  // Copy lottie Folder to /_site
  eleventyConfig.addPassthroughCopy("./src/assets/lottie");

  // Copy swagger styles and js to /_site
  // eleventyConfig.addPassthroughCopy("./src/assets/swagger");

  // Copy swagger data to /_site
  // eleventyConfig.addPassthroughCopy("./src/static/swagger");
  //eleventyConfig.addPassthroughCopy("./src/api/swagger");

  // Copy favicon to root of /_site
  eleventyConfig.addPassthroughCopy("./src/favicon.ico");
  
  // Copy redirection configuration to root of /_site
  eleventyConfig.addPassthroughCopy("./src/_redirects");

  // Minify HTML
  eleventyConfig.addTransform("htmlmin", function (content, outputPath) {
    // Eleventy 1.0+: use this.inputPath and this.outputPath instead
    if (outputPath.endsWith(".html")) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }

    return content;
  });

  // Let Eleventy transform HTML files as nunjucks
  // So that we can use .html instead of .njk
  return {
    dir: {
      input: "src",
    },
    htmlTemplateEngine: "njk",
  };
};
