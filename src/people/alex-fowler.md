---
enabled: true
author: true
name: Alex Fowler
postDate: 2021-08-27T13:54:00-07:00
status: live
jobTitle: Chief Executive Officer
role: Team
id: 31
image: /assets/images/people/headshot-AlexFowler.png
---
An open-source leader and technologist at heart, Alex brings over two decades of experience leading companies and delivering business value in applications of cryptography, privacy, security, and blockchain technology. He co-founded Blockstream, and before that held senior positions with Mozilla, PwC, Electronic Frontier Foundation, Zero Knowledge Systems, and the American Association for the Advancement of Science.