---
enabled: true
name: Jeff Kramer
postDate: 2021-08-27T13:56:00-07:00
status: live
id: 34
jobTitle: Chief Technology Officer
role: Advisor
image: /assets/images/people/headshot-JeffKramer.png
---
Transparent’s former CTO and Paul Allen’s technical advisor with expertise in distributed systems, blockchain, AI, machine learning, and neural networks. Engineering at Vulcan and Microsoft Research.