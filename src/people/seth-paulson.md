---
enabled: true
name: Seth Paulson
postDate: 2021-10-22T14:06:00-07:00
status: live
id: 91
jobTitle: Principal Software Engineer
role: Team
image: /assets/images/people/headshot-sethpaulson.png
---
Lead architect, driving our work on cryptography. Brings over a decade of engineering experience from Vulcan, Changepoint, and Parametric among other companies.