---
name: Brad Gibson
jobTitle: VP, Engineering
role: Team
image: /assets/images/people/headshot-bradgibson.png
---
Delivered major products from inception to market for Microsoft, Microsoft Research, Western Digital, LSI Logic, and Xero. Experienced systems developer, entrepreneur, builder of high-performance teams.