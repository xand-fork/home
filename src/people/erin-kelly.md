---
enabled: true
name: Erin Kelly
postDate: 2021-10-15T13:28:00-07:00
status: live
id: 72
jobTitle: Head of Partnerships
role: Team
image: /assets/images/people/headshot-ErinKelly.png
---
Erin is an experienced growth strategist with partnership experience in payments, venture and SaaS. Partnerships at Crunchbase, Affirm and consulting at Kearney.