---
name: Katie Gray
jobTitle: Deputy General Counsel
role: Team
image: /assets/images/people/headshot-katiegray.png
---
Katie is a seasoned regulatory and compliance attorney with experience at JPMorgan Chase handling investigations and enforcement matters involving various federal and state financial regulatory agencies and advising on compliance policy initiatives. She was previously at PwC and the Fed.