---
enabled: true
name: Patrick Murck
id: 7
jobTitle: President & Chief Legal Officer
role: Team
author: true
postDate: 2021-08-27T13:37:00-07:00
status: live
image: /assets/images/people/headshot-PatrickMurck.png
---
Patrick is a recognized thought leader on fintech and payments. Before joining Transparent, Patrick was Special Counsel at Cooley where he helped develop the firm’s blockchain and Fintech practice. He was a co-founder of the Bitcoin Foundation where he served as General Counsel and Executive Director, and he sits on fintech advisory groups for the NY Fed, MA SEC, and the IMF.