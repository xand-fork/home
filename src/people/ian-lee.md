---
name: Ian Lee
jobTitle: Managing Director, IDEO CoLab Ventures
role: Advisor
image: /assets/images/people/headshot-ianlee.png
---
As head of IDEO CoLab Ventures, Ian invests in blockchain, crypto, and Bitcoin startups. Prior to IDEO, he was a Director at Citi Ventures, Citigroup’s venture capital and venturing arm, where he led Citi’s network of R&D Labs, an early-stage R&D fund, and blockchain across the enterprise from 2014-2017. Ian is an Adjunct Faculty Member in cryptocurrency courses at UC Berkeley.