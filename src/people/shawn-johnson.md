---
name: Shawn Johnson
jobTitle: CEO of AMP Capital
role: Advisor
image: /assets/images/people/headshot-shawnjohnson.png
---
Accomplished global asset management executive, Shawn previously served as Senior Managing Director and Chairman of the Investment Committee for State Street Global Advisors (SSGA), and prior to that as its Director of Global Fundamental Research. Shawn was tapped by Paul Allen to advise on the initial designs for Xand.