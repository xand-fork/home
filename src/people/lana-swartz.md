---
name: Lana Swartz
jobTitle: Assistant Prof, Media Studies, UVA
role: Advisor
image: /assets/images/people/headshot-lanaswartz.png
---
As a critical expert in the emergence of the fintech sector, Lana researches money from the perspective of communication technology. She’s the recent author of New Money: How Payment Became Social Media (2020), based on years of research, interviewing people about money in everyday life, and surfacing the largely forgotten history of payment technologies and industries.