---
name: Chris Brummer
jobTitle: Agnes N. Williams Research Professor of Law and Faculty Director of
  Georgetown’s Institute of International Economic Law
role: Advisor
image: /assets/images/people/headshot-chrisbrummer.png
---
Professor Brummer’s current research focuses on applications of financial technology across derivatives and securities infrastructures and their implications for fintech regulatory policy.