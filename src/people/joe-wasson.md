---
name: Joe Wasson
jobTitle: Director of Product
role: Team
image: /assets/images/people/headshot-joewasson.png
---
Joe is a technical leader with a background in cryptocurrency and sustainable, large-scale engineering. He has nearly two decades of e-commerce and fintech experience with Microsoft, Google, and Stripe, and others.