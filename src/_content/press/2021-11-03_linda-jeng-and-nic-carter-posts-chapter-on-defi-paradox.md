---
title: Linda Jeng and Nic Carter posts chapter on DeFi Paradox
slug: linda-jeng-and-nic-carter-posts-chapter-on-defi-paradox
postDate: 2021-11-03T07:07:00-07:00
status: disabled
source: Risk.net
externalLink: >-
  https://www.risk.net/regtech-suptech-and-beyond-innovation-in-financial-services/7891846/defi-protocol-risks-the-paradox-of-cryptofinance
---
