---
title: How a Digital Dollar Can Make the Financial System More Equitable
slug: how-a-digital-dollar-can-make-the-financial-system-more-equitable
postDate: 2020-07-15T17:07:00-07:00
status: live
source: Coindesk
externalLink: >-
  https://www.coindesk.com/how-a-digital-dollar-can-make-the-financial-system-more-equitable
---
