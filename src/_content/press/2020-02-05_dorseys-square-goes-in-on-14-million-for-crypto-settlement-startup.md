---
title: Dorsey's Square goes in on $14 million for crypto settlement startup
slug: dorseys-square-goes-in-on-14-million-for-crypto-settlement-startup
postDate: 2020-02-05T03:00:00-08:00
status: live
externalLink: https://decrypt.co/18529/dorsey-square-14-million-for-crypto-settlement-startup
source: Decrypt
---
