---
title: >-
  Patrick Murck on CoinDesk's Money Reimagined podcast "New Money and
  Contemporary Capitalism"
slug: >-
  clo-and-president-patrick-murck-is-a-guest-on-coindesks-money-reimagined-podcast-new-money-and-contemporary-capitalism
postDate: 2021-11-22T10:20:00-08:00
status: live
source: CoinDesk
externalLink: >-
  https://www.coindesk.com/podcasts/coindesks-money-reimagined/new-money-and-contemporary-capitalism-with-brett-king-and-patrick-murck/
---
