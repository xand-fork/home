---
title: Cryptocurrency’s Surge Leaves Global Watchdogs Trying to Catch Up
slug: cryptocurrencys-surge-leaves-global-watchdogs-trying-to-catch-up
postDate: 2021-08-21T17:06:00-07:00
status: live
source: Wall Street Journal
externalLink: >-
  https://www.wsj.com/articles/cryptocurrencys-surge-leaves-global-watchdogs-trying-to-catch-up-11629720000?mod=searchresults_pos1&page=1
---
