---
title: As Interest Rates Rise, a Silent Vampire Attack on Crypto
postDate: 2022-08-08T18:37:28.324Z
status: live
externalLink: https://www.coindesk.com/layer2/2022/08/08/as-interest-rates-rise-a-silent-vampire-attack-on-crypto/
source: Coindesk
---
