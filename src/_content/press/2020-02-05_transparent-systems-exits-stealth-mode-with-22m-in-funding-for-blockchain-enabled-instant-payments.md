---
title: Transparent exits stealth mode with $22M in funding for blockchain-enabled instant payments
slug: transparent-systems-exits-stealth-mode-with-22m-in-funding-for-blockchain-enabled-instant-payments
postDate: 2020-02-05T04:00:00-08:00
status: live
source: Geekwire
externalLink: https://www.geekwire.com/2020/transparent-systems-exits-stealth-mode-22m-funding-blockchain-enabled-instant-payments/
---
