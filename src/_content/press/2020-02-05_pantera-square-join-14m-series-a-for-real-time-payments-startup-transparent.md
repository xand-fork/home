---
title: 'Pantera, Square Join $14M Series A for Real-Time Payments Startup Transparent'
slug: pantera-square-join-14m-series-a-for-real-time-payments-startup-transparent
postDate: 2020-02-05T06:00:00-08:00
status: live
source: Coindesk
externalLink: https://www.coindesk.com/pantera-square-join-14m-series-a-round-for-cryptographic-payments-firm-transparent
---
