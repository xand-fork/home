---
title: 'Global Head of Policy, Linda Jeng, moderates panel at LA Blockchain Summit'
slug: global-head-of-policy-linda-jeng-moderates-panel-at-la-blockchain-summit
postDate: 2021-11-01T13:30:00-07:00
status: disabled
source: LA Blockchain
externalLink: 'https://lablockchainsummit.com'
---
