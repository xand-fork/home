---
title: Pantera, Jack Dorsey’s Square join $14M round for crypto settlement startup
slug: pantera-jack-dorseys-square-join-14m-round-for-crypto-settlement-startup-transparent-systems
postDate: 2020-02-05T07:00:00-08:00
status: live
source: The Block
externalLink: https://www.theblockcrypto.com/post/54991/pantera-jack-dorseys-square-join-14m-round-for-crypto-settlement-startup-transparent-systems
---
