---
title: >-
  Commissioner Crenshaw Cites Linda Jeng's Research in Statement on Defi Risks,
  Regulations, and Opportunities
slug: >-
  commissioner-crenshaw-cites-linda-jengs-research-in-statement-on-defi-risks-regulations-and-opportunities
postDate: 2021-11-12T08:18:00-08:00
status: disabled
source: SEC
externalLink: 'https://www.sec.gov/news/statement/crenshaw-defi-20211109'
---
