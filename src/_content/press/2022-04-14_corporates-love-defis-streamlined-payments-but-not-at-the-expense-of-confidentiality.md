---
title: Corporates Love DeFi's Streamlined Payments But Not at the Expense of
  Confidentiality
postDate: 2022-04-14T20:59:02.126Z
status: live
externalLink: https://www.pymnts.com/pymnts-post/news/b2b-payments/2022/corporates-love-defis-streamlined-payments-but-not-at-the-expense-of-confidentiality/
source: PYMNTS
---
