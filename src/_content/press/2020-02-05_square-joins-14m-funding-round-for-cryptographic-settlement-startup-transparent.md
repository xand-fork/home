---
title: Square joins $14m funding round for cryptographic settlement startup
slug: square-joins-14m-funding-round-for-cryptographic-settlement-startup-transparent
postDate: 2020-02-05T05:00:00-08:00
status: live
source: Finextra
externalLink: https://www.finextra.com/newsarticle/35238/square-joins-14m-funding-round-for-cryptographic-settlement-startup-transparent
---
