---
title: Say Hello to Your New Local Bank—The Fed
slug: say-hello-to-your-new-local-bank-the-fed
postDate: 2020-04-13T17:07:00-07:00
status: live
source: Bloomberg Law
externalLink: https://news.bloomberglaw.com/banking-law/insight-say-hello-to-your-new-local-bank-the-fed?context=search&index=4
---
