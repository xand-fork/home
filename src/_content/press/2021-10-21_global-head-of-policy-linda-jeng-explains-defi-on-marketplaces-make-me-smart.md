---
title: Transparent explains DeFi on Marketplace’s Make Me Smart
slug: global-head-of-policy-linda-jeng-explains-defi-on-marketplaces-make-me-smart
postDate: 2021-10-21T17:04:00-07:00
status: live
source: Marketplace
externalLink: >-
  https://www.marketplace.org/shows/make-me-smart-with-kai-and-molly/defi-what-you-need-to-know-about-decentralized-finance/
---
