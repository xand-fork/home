---
title: 'Transparent speaks about crypto pricing, open banking and DeFi'
slug: transparent-speaks-about-crypto-pricing-open-banking-and-defi
postDate: 2021-10-21T16:05:00-07:00
status: live
source: DC Fintech Week
externalLink: 'https://www.dcfintechweek.org/agenda/'
---
