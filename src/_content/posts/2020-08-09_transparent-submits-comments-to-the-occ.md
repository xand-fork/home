---
title: Transparent Submits Comments to the OCC
slug: transparent-submits-comments-to-the-occ
status: live
postDate: 2020-08-09T00:00:00-07:00
teaser: >-
  On August 3rd, 2020, Transparent filed comments with the Office of the
  Comptroller of the Currency (OCC) on digital activities and banking.
authors:
  - transparent
---

On August 3rd, 2020, Transparent filed comments with the Office of the Comptroller of the Currency (OCC) on digital activities and banking.

As a fintech startup, we felt it was important to share what we have encountered so far while working on faster payments, including:  

*   Blockchain-based payment systems can fundamentally change how we control our money and greatly improve financial inclusion.
*   Community banks are falling behind and are not best served by the big core providers.
*   The right levels of API standardization will go a long way in facilitating partnerships between community banks and fintech startups.

The comments are available on the OCC website, but we are republishing them here in full, as well.  

**August 3, 2020**

**Legislative and Regulatory Activities Division****  
Office of the Comptroller of the Currency**  

Transparent Financial Systems, Inc. (“Transparent”) is pleased to provide comments to the Office of the Comptroller of the Currency (“OCC”) in conjunction with the advance notice of proposed rulemaking (“ANPR”) for National Bank and Federal Savings Association Digital Activities.

The mission of Transparent is to build a more inclusive and equitable economy by leveraging technology to advance existing financial infrastructure, while promoting access, encouraging innovation, and reducing costs for all participants. Using cryptography and distributed systems architecture, we have developed the XAND network, a payments settlement solution that empowers small and medium banks to more effectively compete with large financial institutions by retaining deposits and offering on-demand settlement to businesses that bank with them.

Transparent has completed several pilot deployments of the network with a group of financial institutions and intends to bring XAND online in 2020 for a period of production level testing and product-market evaluation. A broader commercial release of the network is expected to follow next year.

The software developed by Transparent can be utilized to support any number of digital dollar settlement applications and the creation of distinct XAND networks. These can be built around discrete “transactional communities” whose members conduct business with one another and wish to benefit from holding their cash a little longer, exchanging value on-demand, settling obligations faster, and reducing risk and costs associated with these activities.  

**General Comments**

We are encouraged by the OCC’s new and ongoing initiatives to explore and support emerging financial technologies (“fintech”) and related business models, and review of current regulations and other guidance that may be hindering innovation developing in the marketplace. We believe receptive regulators and policymakers, with an open door to learning, serves the interests of American customers. Only with an informed regulatory structure can consumers be truly protected and improvements in products and services that help solve real problems, be realized.

We believe that the OCC has taken tremendous strides to respond to innovation with a nuanced, risk-based approach. Again, this started - and continues - with a desire and effort to understand the benefits and risks of new financial products and services. Education is paramount to enacting a workable regulatory regime: we are encouraged by the work of the OCC’s Office of Innovation and the industry collaboration in this regard. We thank the OCC for this dedication to nurture novel solutions, urge them to continue, and urge our industry colleagues to continue to take advantage of this open door.

With the rapid advancement in today’s technologies, such as smartphones, online banking, and cloud storage services, consumers and businesses are demanding and expecting faster payments. This has never been more relevant than it is right now. As the U.S. government seeks to help struggling individuals and businesses suffering from the economic impacts of the COVID-19 pandemic, our payment system has been strained in its ability to match the pace this event has rippled through the U.S. economy.

Faster and more accessible payments enable consumers and businesses to send and receive money within seconds, any time of the day and any day of the year. This allows the recipient to use the funds “almost instantly”, which can make a difference between paying a bill or not, or opening a business. Credit and debit cards are widely used and have been for nearly 20 years, and fintech apps such as Venmo, Square’s Cash App, and Zelle, have become increasingly popular for peer-to-peer payments. Although these services provide immediate access to making payments, it takes at least 1-3 business days before the payment is settled and the recipient can use the funds outside of the services’ “walled gardens.”

The U.S. lags behind many countries with real time payment systems. According to FIS, a total of 54 countries were identified as having an active, real-time payment program. \[1\] That said, only seven countries (Australia, Denmark, India, Poland, Romania, Singapore and Sweden) received the top rating. In the U.S., our largest banks and The Clearing House (TCH) in 2017 launched a new payments consortium called Real Time Payments (“RTP”). However, RTP’s membership consists mostly of the largest banks and covers only half of all U.S. deposit accounts. \[2\] In response, our company’s founder, the late Paul Allen, asked, “why can’t payments be ubiquitous, uniform and accessible to everyone?” To solve this problem, he founded Transparent to develop software solutions that can provide greater, equitable access to on-demand payment and settlement services to more banks, businesses and consumers than what is currently available. Our goal with this solution is to provide a more affordable and efficient option for payments: one that promotes access and innovation, reduces costs, and improves competition across the U.S. financial system by opening up the development of new financial products and services by more businesses.  

**Responses to the ANPR**

The ANPR provides a series of questions, specifically asking whether OCC policies around digital banking are sufficient, what barriers exist for banks in performing activities related to cryptocurrency, how banks use AI, regulatory technology tools, distributed ledger technology, and whether there are other related policy matters that should be clarified through guidance.

We chose to answer select questions from the ANPR, as they are most relevant to our business, our customers, and our understanding of the current and future marketplace.

**Question: What new payments technologies and processes should the OCC be aware of and what are the potential implications of these technologies and processes for the banking industry? How are new payments technologies and processes facilitated or hindered by existing regulatory frameworks?**

The banking industry has long relied on core payment processors and the card networks to support payment services for their clients. New market entrants, such as Cash App or Venmo and bank-owned Zelle, provide customers with peer-to-peer payment options and their market share continues to grow. This growth has only been hastened with the COVID-19 pandemic and a desire by consumers for more contactless payments. \[3\]

That said, these payment options have one thing in common: centralized control and ownership of the underlying infrastructure. Blockchain and distributed ledger technologies facilitate payment tools and protocols that can drastically change the relationship between customer and provider. We believe new blockchain-based payment systems can usher forward a new perception of banking and what it means to control one’s own money. One can already see consumer interest in such systems with the dramatic and sustained rise of so-called “stablecoins,” both in terms of the number of projects, both launched and currently underway, as well as the overall payments volume. \[4\] We believe this shift will result in a move away from a system predicated on gatekeepers and provide a pathway to greater financial inclusivity and a more empowered user.

**Question: Are there issues unique to smaller institutions regarding the use and implementation of innovative products, services, or processes that the OCC should consider?**

While there are a number of innovative products being created by small, non-bank startups, developing relationships between these companies and smaller financial institutions is often viewed as risky by regulators and, in some cases, the institutions themselves. There is a prevailing belief that these institutions do not have the expertise, technical talent, or internal capacity to manage perceived risks around money laundering or other financial crimes. But this perception of smaller instead results in lost opportunities for financial institutions, a lack of access to banking services for non-bank startups, and a loss of solutions that could make financial service more inclusive and accessible for all Americans.

These are precisely the types of risks that technology startups can help mitigate through their advanced services and by lending their expertise. We believe that the OCC should continue its work to ensure that smaller financial institutions feel comfortable in banking smaller technology startups and that those startups are empowered to offer their core competencies to these institutions.

Smaller financial institutions typically have less ability to to quickly adapt to changing business practices. For example, JPMorgan Chase employs 50,000 technologists and invests $11 billion annually on technology, while the local community bank may struggle to deploy a new webpage or integrate mobile banking. Investments to create simple products and services that follow a core set of standards will make it easier for smaller institutions to do business. This will level the playing field, increase marketplace competition, and create more innovative products and services that will benefit the consumer.

We urge the OCC to consider pathways to lower the barrier that some smaller financial institutions face when trying to access fintech marketplaces and services. This can be done by clarifying guidance, strengthening the Office of Innovation’s outreach work, speeding up the process by which opinions and guidance are made, among others. Further, we recommend that the OCC continue to invest in working groups with other financial regulators to pursue cross-agency discussions and identify pain points that can only be solved by whole government approaches.

**Question: What other changes to the development and delivery of banking products and services for consumers, businesses and communities should the OCC be aware of and consider?**

We believe the current system and guidelines puts banks, financial technology companies, and entrepreneurs in a challenging position, limiting their ability to provide positive solutions to businesses and consumers.

The OCC should pursue all options that strengthen bank adoption and use of Application Programming Interface (API) systems. There needs to be clearer guidance and a distinct effort on the part of the OCC and other financial regulators to encourage API adoption. This will not only allow new, innovative products to be developed and launched, but discourage techniques such as “screen scraping” that are widely recognized to be both inadequate and potentially harmful to the consumer.

We urge the OCC and other regulators to additionally support a standardization of API systems, which will further support their adoption, growth and usefulness. The current system by which financial institutions rely on a limited number of core banking providers is severely limiting innovation and American competitiveness. Despite the fact that many financial institutions feel poorly served by these providers, they are “locked in” to these arrangements due to lack of competition and other issues.

We urge a break from old systems and operating procedures, support for new technologies, and the advancement of a financial regulatory system that is principles based, flexible, and recognizes the increasingly open nature of finance. This will increase competition, support a robust market, and help further work to achieve greater financial accessibility and inclusivity for all Americans.

**Conclusion**

Thank you for the opportunity to participate in the comment process. We would be happy to discuss any of the points raised in this letter in more detail and we welcome the chance to provide innovative products and services to our customers.

Linda Jeng

‍Global Head of Policy & Special Counsel  
Transparent Financial Systems

Notes

\[1\] FIS:[Number of Real-time Payment Systems Continues to Grow Globally, FIS Report Shows](https://www.fisglobal.com/about-us/media-room/press-release/2019/number-of-real-time-payment-systems-continues-to-grow-globally-fis-report-shows)(09.18.2019)

\[2\] The Clearing House:[The Clearing House Remains Focused on Operating and Growing the RTP® Network](https://www.theclearinghouse.org/payment-systems/articles/2019/08/tch_remains_focused_operating_growing_rtp_network_08-05-19)(08.05.2019)

\[3\] WSJ:[PayPal Doubles Down as Coronavirus Shifts Payments](https://www.wsj.com/articles/paypal-doubles-down-as-coronavirus-shifts-payments-11596103381)(07.30.2020)

\[4\] IMF:[Digital Currencies: The Rise of Stablecoins](https://blogs.imf.org/2019/09/19/digital-currencies-the-rise-of-stablecoins/)(09.19.2019)