---
title: Xand’s Unique Approach to Payment and Settlement
slug: xands-unique-approach-to-real-time-settlements
status: live
postDate: 2021-02-01T13:30:00-08:00
teaser: >-
  Xands mission is to bring this vision to life for businesses seeking to
  eliminate inefficiencies in their business-to-business (B2B) payment and
  settlement flows.
authors:
  - jeff-kramer
---

**Introduction**  

Transparent was founded by the late Paul Allen with the vision to make money more equitable, efficient, and open. Our mission is to bring this vision to life for businesses seeking to eliminate inefficiencies in their business-to-business (B2B) payment and settlement flows. For example, according to Nacha.org statistics, $41.7 Trillion was transferred between U.S. businesses via ACH in 2020. While the funds are in transit, companies cannot deploy the cashflow into their business or must rely on other financial arrangements to cover capital requirements. This problem is further exacerbated over weekends and holidays.

Newer blockchain-based payment systems are emerging quickly, including ecosystems built around cryptocurrencies and stablecoins. These new systems may meet the need for faster payments and lower costs but introduce added complexity for businesses to manage.

Transparent believes that businesses can benefit from a solution that incorporates the benefits of blockchain technology while operating firmly within the existing regulatory framework. This is why we built Xand – empowering US businesses to collectively operate and govern a payment and settlement network, without a centralized intermediary, and without replacing existing infrastructure.

**Xand Overview**

Xand is a software protocol and governance platform developed by Transparent. Xand empowers businesses to form, operate, and govern a blockchain-based B2B payments and settlement network in order to move value in real time among users. We see groups of transacting businesses and industry verticals as transactional communities.

Transactional communities can be businesses with shared interests and needs that conduct business regularly with each other. For example, multiple trading firms and brokerages that frequently trade and settle with each other can be defined as a transactional community. Or a consumer brand with a network of wholesalers and suppliers can be defined as another transactional community.

Xand enables these transactional communities to create member-owned limited liability companies, which we call a Xand Member Collective Organization (XMCO), and a trust (Xand Trust), allowing them to operate a payments and settlement network (Xand Network). As members of a Xand Network, businesses exchange value with other businesses and users on a network in real time, 24 hours a day, year-round using a digital dollar payment platform upon which members can send each other value in Xand Claims. Designed to operate within existing U.S. legal and regulatory framework, claims are USD-denominated, digital representations of funds held in trust at participating Xand-Enabled banks (XEB). Members’ existing banks can become Xand enabled by providing account access via existing APIs.

**Xand Network Structure**

A Xand Network consists of the following participants that play different roles.

*   **Members.** Businesses, organizations, or other legal entities who may already transact with each other join a Xand Network as members of XMCO. Members are the primary users of Xand and utilize Xand software to request the creation or redemption of Xand Claims, and to transfer value in real time by transferring Xand Claims to other members. Additionally, members vote on Network rules, investment objectives for funds held in trust, and add new members.
*   **Xand-Enabled Banks (XEB).** To become a XEB, a bank supplies basic Application Programing Interface (API) functionality to support network requirements, including account balance checks, internal book transfers, and transaction history. Banks do not need to install any proprietary software because Xand software interfaces with the banks’ existing APIs_._
*   **Trustee.** The Trustee is an independent third party named by XMCO as Trustee of the Xand Trust. The Trustee exercises fiduciary powers and fulfill certain administrative and operational functions on its behalf. The Trustee’s role includes safeguarding trust assets, paying network expenses, and facilitating claims creation and redemption processes.
*   **Validators.** Validators operate nodes on the Xand blockchain, validate transactions, bundle the transactions into blocks, and publish blocks to the blockchain. Validators also engage in Network governance alongside members. Validators receive payment for operating the software nodes and authoring blocks. Validator compensation rates are collectively determined by the members of XMCO.
*   **Transparent**. Transparent licenses to members, validators, and the trustee the technology relating to Xand. ​

**Transaction Lifecycle**  

Members use Xand software to create, send, and redeem Xand Claims as the mechanism for exchanging value in real time. Members are the only network participants that can (i) request the Creation of Claims, (ii) Transfer Claims to other members, and (iii) request the Redemption of Claims.

For a more detailed description of each step in a transaction lifecycle, please refer to [Transaction Lifecycle on a Xand Network.](https://transparent.us/assets/images/posts/TransactionLifecycle_new.pdf)

**Xand Design Principles**

Xand is designed in consultation with leading experts in cryptography and financial regulation. The product design objective was to stay true to several over-arching principles summarized below. In a later blog post, we will discuss why these principles are so important for a B2B digital dollar payment platform.

*   design to operate within existing U.S. legal and regulatory frameworks
*   be bank agnostic by leveraging, not replace, existing banking infrastructure and relationships
*   empower users (businesses) to govern their own payments networks
*   deliver speed and scalability through blockchain technology
*   implement strict confidentiality and privacy standards
*   minimize or eliminate transaction costs
*   make it easy for businesses to implement and operate

**Technology**

Xand is built using Rust, a programming language designed for safety and correctness. We developed a privacy framework that keeps transactions confidential. We accomplish this by using zero-knowledge proofs and highly tested cryptography primitives. The result is that only transacting participants know the details of a transaction.

Each Xand Network participant receives its own software, designed for its role. The software can run in a wide variety of environments - in any of the major clouds, on-premises, co-located, or hybrid, on Windows, Linux, or OSX. Xand software can be installed and configured over the course of a few hours by a single engineer.

For a member, the interface to Xand is a simple REST API - represented in Swagger/OpenAPI to enable easy client development. Prospective members can test drive Xand by accessing Xandbox, a full system sandbox to build integrations of specific requirements and to test Network designs.

Xand’s technical design choices result in an anti-fragile network - the more participants that join the Network, the better it functions and the more resilient it becomes.

In the end, our goal is to remove as much complexity as possible from the installation, configuration, operation, and maintenance of a faster payments platform, while simultaneously maintaining the security and resiliency.

**Summary**

Our mission is to empower companies to unlock new business opportunities with a Xand payment and settlement network. Transparent, as the developer of Xand software, will be there every step of the way from concept development through implementation.

**Talk to us to see how Xand can address for your payment and settlement needs today!**

**Contact**

Erin Kelly | Head of Partnerships | Transparent Financial Systems | [erin@transparent.us](mailto:erin@transparent.us)