---
title: The Team at Transparent is Growing
slug: the-team-at-transparent-is-growing
status: live
postDate: 2021-09-10T13:05:00-07:00
teaser: >-
  Announcement of new senior hires to Transparent as we ramp up for the next
  stage of delivering a new digital dollar platform and product to the market.
authors:
  - alex-fowler
---

We have a lot of exciting things in the works. Today, I am excited to share some news about two new senior people joining the Transparent team: **Erin Kelly**, Head of Partnerships; and **Joe Wasson**, Director, Product. They are joining as we ramp up for the next stage of delivering a new digital dollar platform and product to the market. I’ve included brief bios summarizing their impressive backgrounds, accomplishments, and job experience below.  

#### Erin Kelly  
Head of Partnerships

Also joining Transparent this month, Erin is heading up partnerships, joining from Crunchbase where she ran their venture program and oversaw the launch of that company’s newest software product. A consummate relationship builder focused on delivering growth through business development, she has established partnerships with companies spanning lending institutions, venture capital firms, software platforms and nonprofits, including organizations such as Techstars, All Raise, Outreach, and WooCommerce. She previously worked in strategic partnerships at Affirm and as a management consultant at Kearney. She holds an MBA and MA from Yale, and a BA from UC Berkeley. \[[LinkedIn](http://linkedin.com/in/erinkelly3)\]

#### Joe Wasson  
Director, Product

Joe joined Transparent in May as a Principal Engineer and then transitioned to our Director, Product in a few months. He was our first new hire since the start of the pandemic and brings to the company an impressive background in cryptocurrency and sustainable, large-scale engineering. He has more than two decades of experience in e-commerce and FinTech, beginning with Microsoft where he worked on several e-commerce projects including building core systems for the initial launch of store.microsoft.com. He then honed his skills at Google before transitioning to FinTech and leading the payments teams for Xero and Splice and the tax platform team for Stripe. Joe has extensive blockchain experience working as a freelance cryptocurrency consultant and smart contract auditor. \[[LinkedIn](https://www.linkedin.com/in/joewasson/)\]

Congratulations to Erin and Joe, and welcome to the team!