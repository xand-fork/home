---
title: Transparent Unveils a B2B Confidential Payment System 
slug: transparent-unveils-a-b2b-confidential-payment-system 
postDate: 2022-3-25T10:00:00-08:00
status: live
authors:
  - seth-paulson
teaser: Transparent has developed a confidential blockchain based b2b payment system
---


With the launch of Xand Alpha we would like to present one of its key features: confidential transactions. Transparent has developed a confidential-transaction system drawing from proven cryptography in the field. This system allows users to protect their identity and transaction details while preserving the public auditability of the ledger. If you’d like to know how this system works in full detail download our [comprehensive confidentiality white paper](/assets/documents/Confidentiality%20White%20Paper%20-%202022-02-24.pdf). In this blog post we will explain why this feature is so important.  

Advances in blockchain technology represent a huge innovation in financial systems and reporting. The technology enables anyone to maintain a complete copy of every transaction and verify its validity. This provides a previously unheard-of amount of transparency in a financial product, not to mention other benefits like decentralization, no single points of failure, immutability, and trustlessness. This level of transparency, however, has an inherent downside: companies using a blockchain are literally publishing their accounting records.  

Businesses are accustomed to having their financial transactions remain private. Exposing this financial information can reveal information about a business’ suppliers, business partners, sales, and trading volumes and patterns. This could be a strategic disadvantage for a business and discourage them from making use of a blockchain based system. 

What can be done about this? One option is to take advantage of the pseudonymous nature of most blockchain systems and create many identities. Even a new one for every transaction. This is not a viable option for a Xand network as the set of participants is closed and known to all parties.  

Thankfully, modern cryptography has provided better tools. Zero-knowledge proofs allow proving knowledge of information without revealing it and homomorphic encryption schemes allow performing math on numbers when you don’t know what they are. Clever combination of these techniques has allowed the construction of privacy focused cryptocurrencies.  

Transparent’s research team has studied these systems and adopted the best techniques from them to build a system that fulfills the requirements needed for business users of a Xand network. The identities of the transacting parties and the amounts involved are kept private. However, unlike some confidential systems, Xand’s design helps to facilitate compliance. The system ensures that users always know the counterparties to any transaction they are involved in. Additionally, users can provide proof to any third party about the hidden details of a transaction. The third party is then able to verify this claim by looking at the shared ledger. 

To find out more about how our confidentiality system works [download the Whitepaper](/assets/documents/Confidentiality%20White%20Paper%20-%202022-02-24.pdf). 
