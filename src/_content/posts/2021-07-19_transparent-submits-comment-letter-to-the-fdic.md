---
title: Transparent submits comment letter to the FDIC
slug: transparent-submits-comment-letter-to-the-fdic
status: live
postDate: 2021-07-19T00:00:00-07:00
teaser: >-
  On July 15, 2021, Transparent filed comments with the Federal Deposit
  Insurance Corporation (FDIC) in response to its Request for Information and
  Comment on Digital Assets. As a fintech startup, we felt it was important to
  share what we have learned so far while working on faster payments.
authors:
  - transparent
---

On July 15, 2021, Transparent filed comments with the Federal Deposit Insurance Corporation (FDIC) in response to its Request for Information and Comment on Digital Assets.

As a fintech startup, we felt it was important to share what we have learned so far while working on faster payments:

*   To help promote innovative digital dollar payment solutions in the US, the FDIC and other federal financial regulators should carefully consider the overall taxonomy of “digital assets.” Clearly distinguishing the term “digital dollars” from the term “stablecoins” is essential for legal and regulatory clarity and would the healthy growth of digital dollar payment systems.
*   Digital dollars are digital representations of the US dollar, are fully backed by US dollars and do not require centralized price stabilization measures. They should convey the same legal rights as dollars held in custody.
*   Enabling reliable technological infrastructure like Application Programming Interfaces (APIs) is a necessary component for banks, especially community banks, to operate in the new digital landscape and have the capacity to work with third parties that provide innovative financial services and products. This will ultimately enable growth across all institutions in the industry.  
    

The comments are available on the FDIC website, but we are republishing them here in full, as well.  

We respectfully submit this letter in response to the FDIC’s Request for Information (RFI) and Comment on Digital Assets. Our comments below focus on the following areas:

*   The term “digital assets” is very broad and encompasses a number of different types of instruments, many of which include different product structures and serve different purposes. Covered under this term, we recommend that the FDIC clearly distinguish the term “digital dollars” from the term “stablecoins” and underlying taxonomy while considering regulations or guidance related to digital assets.
*   Enabling technological infrastructure like application programming interfaces (APIs) is a necessary component for banks, especially community banks, to operate in the new digital landscape. Such capacity is necessary for working with financial technology firms that provide innovative financial services and products. This will ultimately enable growth across all institutions in the industry.
*   FDIC should enhance its educational efforts to ensure that all FDIC staff understand how these various instruments function in the evolving financial sector landscape.  
    

As background, Transparent Financial Systems, Inc. (Transparent) is a technology software provider. Transparent is developing a B2B digital dollar payment solution called Xand, which leverages distributed ledger technology (DLT) to make payments more efficient, secure, and equitable. Our digital dollar product Xand is designed for transactional communities of businesses that regularly transact and do business with each other.

As US businesses find all too familiar, cash payments generally take at least one to five business days to settle in the US. The lengthy settlement times result in costly delays and inefficiencies for US businesses. A few faster payment systems do exist, but they require high entry costs that price out many smaller insured depository institutions (IDIs).

Against this backdrop, we have developed a DLT-enabled digital dollar payment solution called Xand that gives transactional communities, which are groups of businesses that transact frequently with each other, the tools to establish their own respective payment networks. Through Xand, businesses can use permissioned, decentralized networks that do not require expensive central intermediaries to send value in real-time and settle on-demand.

Xand also facilitates the building and retention of deposits on the balance sheets of Xand-supporting banks, which hold cash reserves backing the digital dollars. We have also designed Xand, so banks can use their own APIs to communicate with Xand networks – no proprietary software required.  

We do all this while adhering to financial regulatory requirements. Because of our role in digital dollar innovation, we have made some observations with a direct view of how businesses and IDIs can use digital dollars.  

**Taxonomy of Digital Assets**

Before we address some of the questions in the RFI, it is important to take the opportunity to discuss the taxonomy of digital assets, as we see them in the industry. We have observed the emergence of many different terms that could fall under the nomenclature of “digital assets,” including “digital dollars”, “stablecoins”, “cryptocurrencies”, “cryptoassets”, “central bank digital currencies”, and “tokens”, to name a few.

Many of these new terms do not have official definitions, and some terms are simply incorrect or misleading. For example, many so-called “stablecoins” do not even have stable prices. However, the term “stablecoin”, for example, potentially covers many different types of instruments. Some stablecoins are fully backed by reserves. Others only partially backed. A few, such as algorithmic stablecoins, are not backed by reserves at all. The reserves, themselves, can be any combination of fiat currencies, treasuries, cryptocurrencies, or other types of assets. Some of these stablecoins could very well be securities under US securities laws.

Conversely, compared to stablecoins, “digital dollars” are digital representations of the US dollar. They are fully backed by US dollars and do not require centralized price stabilization management or control.

Drawing a clear distinction within the digital assets taxonomy is necessary for legal and regulatory clarity and is essential for the healthy growth of digital dollar-based payment systems. Absolute legal and regulatory clarity for digital dollars is important for a number of reasons, including:

1.  Private commercial law

The Uniform Commercial Code (UCC) harmonized state laws beginning in the 1890s and views assets in essentially three categories: money, tangible assets and intangible assets. Tangible assets include land, equipment, inventory, etc., and are physical in nature as opposed to intangible assets, which are not physical in nature but have potential monetary value, such as intellectual property and goodwill. Tangible assets also include financial assets, such as stocks, bonds and cash, which are categorized as current assets because they can be readily sold for cash.

As a type of digital asset, stablecoins that are securities would be subject to Article 8 of the UCC, which governs how securities are held and transferred and how ownership and other interests in securities can be recorded and changed. Interestingly, loans secured by stablecoins would be subject to Article 9, which governs secured transactions. This means a senior creditor’s lien on a stablecoin would travel with such a stablecoin to the new owner unless the stablecoin were a security and the new owner acquired the security from a securities intermediary, which is usually an SEC-registered clearing corporation or broker-dealer.

Money, on the other hand, is treated quite differently under the UCC, which defines “money” as “a medium of exchange currently authorized or adopted by a domestic or foreign government as a part of its currency.” Thus, money is usually regarded as tangible physical cash and not as rights against a depository bank represented by funds in a deposit account. A creditor can perfect a security interest in cash posted as collateral only if the creditor possesses the cash or controls the deposit account holding the cash. If the creditor does not possess the cash or control the deposit account, it loses its perfected interest in the cash collateral.

In other words, a creditor’s lien cannot travel with money. If you offer money to pay for something, the seller must accept it as a means of payment and not worry if a third party might have a claim to that money. This framework allows money to be used as a medium of exchange that is fungible, durable, recognizable and divisible.

1.  Financial regulation

Defining whether a digital dollar is cash equivalent or another type of asset could also affect the IDI’s ability to hold digital dollars on their balance sheets or hold them even as custodians. If digital dollars are not considered to be cash equivalent, then banks would be required to hold capital against digital dollars. The Basel Committee’s Consultative Document on the prudential treatment of cryptoasset exposures states that such “cryptoassets must confer the same level of legal rights as cash held in custody.”  

But if digital dollars are deemed to be securities under US securities laws, then they would be subject to securities disclosure, prospectus, listing and trading rules, making digital dollars useless as a medium of ready exchange.

1.  Accounting

For similar reasons as those described above, if digital dollars are not considered to be cash equivalent than there could be double counting of the dollar reserves and their digital representations.

Accordingly, if we want virtual currencies, such as the digital dollar, to work as a medium of exchange and support innovative payment solutions, we need to ensure our laws and regulations clearly recognize digital dollars as money. Otherwise, digital dollars cannot be fungible, divisible, and transferable like money. In addition, the knock-on effects in accounting treatment, bank capital requirements, securities regulation, etc., would destroy the incentives in ever using digital dollars as an efficient means of payment. Thus, regulators need to take great care when developing their taxonomies of the digital assets world and correctly distinguish digital dollars from stablecoins and other digital assets because a digital dollar is still a dollar.  

**Responses to FDIC RFI Questions**

While we are not an institution regulated or supervised by the FDIC, we would like to comment narrowly on the potential IDIs and their affiliates have with digital dollar solutions like Xand. We have addressed specific questions in the RFI based on our viewpoint of the industry as a whole. We will not be commenting on all questions posed.  

_4\. To what extent are IDIs’ existing risk and compliance management frameworks designed to identify, measure, monitor, and control risks associated with the various digital asset use cases? Do some use cases more easily align with existing risk and compliance management frameworks compared to others? Do, or would, some use cases result in IDIs’ developing entirely new and materially different risk and compliance management frameworks?_

Transparent designed our product Xand to fit within the current legal and regulatory framework. While we eliminate many of the intermediary roles in the payments ecosystem, we ensure that our payments solution adheres to the legal and regulatory requirements necessary to complete the payment transaction. As we partner with businesses and banks to enable these payments, we ensure that they have the correct technology in place to enable Xand networks. As such, the existing frameworks of risk and compliance apply, just as if the IDI is enabling a normal payments transaction. We do not see a need to change existing regulation to accommodate the new payments structure we have developed. However, there may be a need to more clearly define “digital dollars” vis-à-vis “stablecoins” (as discussed above) and offer additional opportunities for training of FDIC staff to highlight how these new instruments fit into the current legal and regulatory structure and the evolving financial sector. We outline this in more detail below.

_8\. Please identify any potential benefits, and any unique risks, of particular digital asset product offerings or services to IDI customers._

As mentioned above, Xand facilitates the growth and stickiness of deposits on the balance sheets of Xand-supporting banks, which hold cash reserves backing the digital dollars of Xand networks. We have also designed Xand, so IDIs do not need to use proprietary software to work with Xand networks. They only need basic, reliable APIs.  

_7\. How are IDIs integrating, or how would IDIs integrate operations related to digital assets with legacy banking systems?_

From our experience, many smaller IDIs, especially community banks, do not have the level of resources or technology know-how to build their own APIs. They often do not have sufficient resources for IT, dedicated technical staff, developers interested in working on legacy core systems, etc. Thus, they must purchase APIs from their core providers - with whom they are usually locked in multi-year contracts. The APIs they purchase are often expensive and of insufficient quality (poor connectivity, poor problem diagnostics, etc.) to interface with fintech firms, the new open banking environment, and DLT-based platforms. The poor quality of these APIs may be rooted in the broader context of technical stagnancy and system limitations at core providers. To advance a vibrant open banking system, the issue of APIs needs to be addressed, but APIs are but a component of a greater list of issues for community banks, including costs, resourcing, expertise and being captive to proprietary, legacy systems.

Additionally, many community banks are uncertain about working closely with fintechs - like Transparent - with products and services that communicate with banks via APIs. As the FDIC decides to move forward after this RFI, ensuring confidence in technological innovations like APIs to enable IDIs to participate more meaningfully in the new digital financial environment will be critical to moving the financial services industry forward and ensuring growth for all IDIs.

_11\. Are there any areas in which the FDIC should clarify or expand existing supervisory guidance to address digital asset activities?_

Additionally, as we previously mentioned, a clear taxonomy is needed to better describe the various instruments that are included under the term “digital assets.” Further defining the different types of digital assets will provide clarity and confidence to IDIs on the types of activities utilizing these instruments.

Last but not least, we recommend increasing the FDIC’s internal education efforts to ensure a deeper understanding of how digital assets fit into the current regulatory structure. Transparent would be happy to offer our expertise to help in these educational initiatives.

Linda Jeng

‍Global Head of Policy & Special Counsel  
Transparent Financial Systems