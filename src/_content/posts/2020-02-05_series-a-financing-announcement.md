---
title: Transparent Secures Series A Financing
slug: series-a-financing-announcement
status: live
postDate: 2020-02-05T00:00:00-08:00
teaser: >-
  Weve kept mostly out of public view and been focused on delivering real,
  concrete results to our initial customers and partners. However, I’m really
  excited to announce today that we closed over $14 million in Series A
  financing. Led by Pantera Capital with participation from Square, Inc.,
  Future\Perfect Ventures, IDEO Colab Ventures, Digital Currency Group, and CMT
  Digital, the funding round is in addition to the $8 million in seed financing
  received from Vulcan Capital in July 2018.
authors:
  - alex-fowler
---

SEATTLE, WA, Feb 5th, 2020 – Transparent Financial Systems, Inc., a Seattle-based startup developing on-demand, 24/7/365, cryptographic settlement solutions, announced today it has closed over $14 million in Series A financing. Currently concluding its beta program with a number of major financial services and fintechs in the US, Transparent will use this round of funding to accelerate product development, engineering, and begin overseas expansion.

Led by Pantera Capital with participation from Square, Inc., Future\\Perfect Ventures, IDEO Colab Ventures, Digital Currency Group, and CMT Digital, the funding round is in addition to the $8 million in seed financing received from Vulcan Capital.  

Transparent began at Vulcan Inc. at the direction of the late Paul Allen, along with Shawn Johnson, CEO at AMP Capital and the former chairman of the investment committee of State Street Global Advisors. Transparent was spun out by Jeff Kramer and members of the team at Vulcan. The company hired industry veterans Alex Fowler as its CEO and Patrick Murck as its Chief Legal Officer.  

“The financial industry is ready for new technology to improve the way people pay for goods and services,” said Chris Orndorff, Chief Investment Officer, Vulcan Capital. “The team at Transparent has made impressive strides over the course of its first year, and we are excited to continue our work with them as their strategic partner and new investor that honors Paul Allen’s vision for the next wave of fintech.”  

Pantera and Future\\Perfect will also join Vulcan on Transparent’s Board of Directors.  

“Blockchain technology is rooted in decades of advances in distributed systems and cryptography, and it’s at the center of banking and financial innovation today,” said Joey Krug, Co-Chief Investment Officer, Pantera. “Based on Transparent’s strategic partnerships and solution, we believe this investment represents an exciting opportunity and will provide tremendous value to the future of on-demand settlements.”  

"The financial services industry is undergoing significant transformation through the application of new technologies,” said Jalak Jobanputra, Founding Partner at Future\\Perfect and one of today’s most influential fintech leaders. “We are thrilled to back Transparent in creating a more efficient, equitable, and resilient financial system. We look forward to working with them as they enter this exciting new phase of growth."  

“Square believes that blockchain represents a path towards a more robust, safe, and empowering future for all participants in the economy – from individuals to businesses,” said Mike Brock, Strategic Development, Cash App at Square. “We are excited to work with Transparent to bring that vision to the financial system.”  

Transparent achieved several key milestones, including demonstrations of on-demand settlement of USD between multiple financial services firms, fintechs, and banks. The company is on track to open up its network for evaluation, testing, and initial commercial use.