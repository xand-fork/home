---
title: Hello World
slug: hello-world
status: live
postDate: 2020-02-05T00:00:00-08:00
teaser: >-
  We are excited to finally announce that Transparent is here. We left Vulcan,
  started the company, and brought together an experienced, interdisciplinary
  team of over 20 people. We got a pilot version of our network up and running.
  In addition, we closed over $14M in our Series A round of funding right before
  the holidays.
authors:
  - jeff-kramer
---

Envisioned by the late Paul Allen and spun out of Vulcan, Transparent is building seamless on-demand 24/7 settlement solutions. Leveraging cryptographically-secured distributed networks, we’re reducing the friction in making and receiving payments.

We are excited to announce Transparent. After we left Vulcan, we started the company, and brought together an experienced, interdisciplinary team of over 20 people. We got a pilot version of our network up and running. In addition, we closed over $14M in our Series A round of funding right before the holidays.  
  
**Where We Started**  

Paul Allen said, “The possible is constantly being redefined...” and he cared deeply about helping society advance, whether it was social, ecological, or financial problems. One of his longest standing goals was to modernize the antiquated infrastructure of money: slow to evolve, regressively costly, fragmented, and too complicated to access.

He recognized in today's on-demand economy built of 1s and 0s, technology marches forward yet crucial financial systems lag behind and remain plagued with legacy issues. Massive amounts of money continue to be tied up daily due to settlement time constraints. Banking fees add up to sums comparable to the GDPs of many small countries. The costs incurred by these systems and single points of control contribute to the lack of access to financial services for a large portion of businesses and people, both here in the US and around the world.

Solving these enormous problems is what motivated us to find a solution. Together with Paul, several other colleagues at Vulcan, and a number of external experts, we worked through a range of possible system designs, use cases, and architectures. In July of 2018, Transparent received $8 million in seed funding from Vulcan Capital to further develop one of those innovative solutions into a network to improve the ease and efficiency of payments today.  

**Finding the Right Team**

Vulcan introduced us to Shawn Johnson, who is now a member of our advisory committee and served as our interim CEO, to work with us on the initial design of the network. Previously, Shawn was the Chairman of the Investment Committee at State Street Global Advisors and ran State Street’s Advanced Research Center and Product Engineering. Shawn’s input continues to prove to our understanding of the requirements necessary to support and seamlessly integrate into the existing financial ecosystem.

In December of 2018, we hired Alex Fowler as our CEO. Before joining Transparent, Alex co-founded Blockstream, which built on his previous expertise in open source and blockchain technologies, applied cryptography, privacy, and security. Previously in his career he held leadership positions with Mozilla and PwC, as well as at Zero-Knowledge Systems, the Electronic Frontier Foundation, and AAAS.

In May of 2019, Patrick Murck joined as our Chief Legal Officer. He was previously Special Counsel at Cooley LLP, focused on cryptocurrency and blockchain technology. He’s affiliated with the Berkman Klein Center for Internet & Society at Harvard University and is a member of the IMF’s High-Level Advisory Group on Fintech. Patrick is also a member of the Fintech Advisory Group for the Federal Reserve Bank of New York and Massachusetts Securities Division.

In addition to Alex and Patrick, we have been incredibly fortunate to have a cadre of talented folks join our team from prominent tech and financial companies throughout the US and the Pacific Northwest. Together we comprise a wide breadth and depth of expertise, including legal and compliance, product, design, as well as software, networking, and cryptography.  

**Moving Money Forward**

Our first focus is on B2B payments and treasury automation, making it possible for companies to directly manage settlement while lowering risk and costs.

We create solutions, not currencies. Our goal is to create a more inclusive economy that empowers your business and your money.

We’ll have more to share about our work in the months ahead, and we’re looking forward to opening the network for evaluation, testing, and initial commercial use later this year.

If you are excited by the prospect of improving payments, open banking, distributed systems, cryptography, and the future of finance, please get in touch with us.

If you are interested in joining our team visit our website at [transparent.us](https://www.transparent.us/) and check out our career opportunities.