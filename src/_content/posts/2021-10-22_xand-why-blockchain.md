---
title: 'Xand: Why Blockchain?'
slug: xand-why-blockchain
status: live
postDate: 2021-10-22T14:06:00-07:00
teaser: >-
  Transparent’s payments platform, Xand, is built on blockchain technology, the
  same foundation of most cryptocurrencies.
authors:
  - seth-paulson
---

Transparent’s payments platform, Xand, is built on blockchain technology, the same technology used as the foundation of most cryptocurrencies. Unless you’re a hermit living off the grid (in which case thanks for reading this post), you’ve almost certainly heard of blockchains and cryptocurrencies. Even my grandmother has called to talk to me about the price of bitcoin. The world is abuzz with news and predictions: some are in love with crypto and think it will solve all the world’s problems, while others are in love with hating crypto and think it is nothing but a fraud.

Blockchain technology is a transformative innovation that is changing the world. However, as with many transformative technologies, it’s crucial to ensure that blockchain is the right solution and not of interest just because it’s currently popular. In this first of two posts, I will outline why we chose blockchain technology to build Xand.

**What We Believe**

At Transparent we believe in choosing the best tool for the job. Any technology choice needs to be justified as the best one for the requirements of the product we’re building. We do not believe in adopting technologies just because they are cool or popular. In fact, a technology with a lot of hype around it is a reason for extra skepticism. Following the hype train is not the way to build sustainable value for your customers. As a result, when we designed the Xand platform, we had to carefully analyze whether blockchain was a good fit.

Xand is designed as a digital-cash payments platform. It’s a way for Members of a Xand Network to transact with each other using a digital dollar. Because of the peer-to-peer nature of each Network, decentralization— or the removal of centralized control— was a crucial element in the design. This eliminates single points of failure and the need for absolute trust of other entities. It also empowers Network Members to have full control over their own funds.

Our advisor Lana Swartz pointed out another requirement with her insightful definition of money, which she said was a “technology of memory”. That meant that the Xand platform required some sort of ledger to serve as the collective memory of who owns how much money — just like a classic bank ledger. To enable the usage of digital cash, this ledger of course needed to be digital. To support our first requirement of decentralization, it also needed to be updatable by any Member, not controlled by any single party.

In furtherance of these requirements, we investigated various methods of maintaining a digital ledger.

**Why Not a Database?**

Banks have used traditional databases to maintain their ledgers for a long time. These databases (such as mySQL, SQL Server, and Postgres) are a mature technology; they are highly performant and reliable. However, traditional databases are centralized and use a command-and-control model where a single node is authorized to write data to the database. This violates the decentralization requirement for our network because it requires absolute trust in a single entity. This entity would be able to dictate what information got written to the ledger and could control how other participants read from it. Additionally, if that centralized entity were to have technical issues, it would bring down the entire network. Because we are creating networks of peers, and because we want to ensure the networks are highly reliable, this was not a viable solution for the Xand Network.

**Why Not Modern Distributed Stores?**

The need for modern systems to improve availability and scaling has led to a proliferation of different kinds of distributed stores. These systems have various levels of centralized control and tolerance of node failure. However, none of these systems are designed to tolerate malicious control of nodes.

In the best case this means that a malicious node might make denial-of-service attacks and in the worst case they might reorder, delete, or write arbitrary new data to the ledger. Worse, every new participant adds additional risk to the network! This works directly against Xand’s goal of peer-to-peer interactions, so distributed-store technologies also proved unsuitable for the Xand Network’s ledger.

**Blockchain to the Rescue!**

That’s what brought us to blockchain. Fundamentally, it’s a way for multiple parties to agree on an ordered list of verifiable commitments. Not only have cryptocurrencies proven that a money ledger can be easily expressed in this form, but this conclusion is supported by hundreds of years of accounting history, which have demonstrated that an irreversible ordered list of transactions is the best way to accurately keep such a ledger.

Blockchain technology also meets our requirement for peer-to-peer interactions because it was designed to operate in the completely trustless environment of the public internet and to tolerate all kinds of malicious behavior. In fact, it provides more protection than we needed: by carefully designing our network onboarding and incentive structure, we can tolerate a minimal level of trust between Xand Network Members which allowed us to replace Proof of Work— which has proven costly for many blockchain network— with Proof of Authority. The result is a truly distributed network that shares control among its Members and can scale the number of nodes without increasing risk.

In the end, blockchain was not just the best technology with which to build our product, but the only one that met all our requirements! However, that’s still just half the problem. Even after deciding to use blockchain technology, we still had to determine how to incorporate it into the Xand platform. Our next blog post will discuss how we made that decision.