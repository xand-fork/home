---
title: Build a Castle in the Xandbox
slug: build-a-castle-in-the-xandbox
postDate: 2021-09-10T13:04:00-07:00
status: live
authors:
  - jeff-kramer
teaser: Transparent is proud to announce Xandbox, our developer toolkit.
---
Transparent is proud to announce Xandbox, our developer toolkit for building on top of our Xand protocol and codebase used to establish B2B cash settlement networks. Developers or others interested in the technology can [contact us](mailto:xandbox@transparent.us) to begin immediate integration.

![](/assets/images/xandbox-red.png)

\
Xand is a real-time, decentralized settlement network that gives US businesses the opportunity to own and control their own payment networks that are seamlessly integrated with their banks via APIs. Built with Substrate and Rust, Xand is secure and fast, representing state-of-the-art distributed systems engineering.

Through working with Xandbox, you and your colleagues at your business will get a hands-on view into Xand. Xand allows you to build and join settlement communities, or a group of counterparties focused on your business networks, to make payments instantly. These communities are managed collaboratively, without gatekeepers, ensuring that your needs are always the priority. The payments in a Xand network occur as Xand Claims or cryptographic tokens redeemable on demand for US dollars, so there’s none of the volatility risk of cryptocurrencies or the centralization of fiat-backed stablecoins. Best of all, the cost of operating the network is paid for by interest on the funds that are in active use, creating an architecture that’s better-than-free: it actually pays to pay.

Your business or group of counterparties can now test out the Xand network in Xandbox, a sandbox of local testnets that allows you to thoroughly analyze how Xand works, model out setups and integrations, and test configurations before deploying into your production environment. The setup is easy, with full operating instructions for custom installations using docker-compose.

Xandbox includes a complete OpenAPI (Swagger) interface, allowing you to quickly test all the API functions used in production by Xand users. This is not a mockup or a specification. Even better, it’s always up-to-date with the latest shipped codebase: Xandbox is the first place to see Transparent’s newest work on Xand. When you’re ready to deploy, the provided API is identical to the production network API, which makes it easier to move your treasury management real-time prototypes into production.

Each Xandbox includes:

* A fully specified chain
* A set of five Validator nodes, which provide consensus for the chain updates
* A set of two Member nodes, complete with Member APIs
* A set of mock bank APIs, based on the API wrapper designed by Transparent for Xand Enabled Banks
* A Trust Node, performing the on and off chain operations to Create and Redeem Claims

If you’re ready to test out the simple-to-install and simple-to-run Xandbox, so that you can be among the first wave of developers running B2B payments that are fast, final, and free to transact, contact us at Transparent to immediately begin a no-obligation integration. You can email us at [xandbox@transparent.us](mailto:xandbox@transparent.us) to be onboarded.