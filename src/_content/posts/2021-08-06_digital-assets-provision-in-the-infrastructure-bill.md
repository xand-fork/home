---
title: >-
  Transparent sends letter to Senators on the digital assets provision in
  Infrastructure Bill
slug: digital-assets-provision-in-the-infrastructure-bill
status: live
postDate: 2021-08-06T00:00:00-07:00
teaser: >-
  Transparent recently sent a letter to Senators Wyden, Lummis and Toomey
  regarding the digital assets provision in the Infrastructure Bill.
authors:
  - transparent
---

Dear Senators Wyden, Lummis and Toomey,

Thank you for your recent amendment to the Infrastructure Bill. We are a fintech startup founded by the late Paul Allen of Microsoft. Our goal is to make money easier and cheaper to access, which has led us to develop a DLT-enabled digital dollar payment solution for American businesses.

We believe that American companies and persons should pay their fair share of taxes to support our country. We also find the bill’s citation of the digital assets industry to be a vote of confidence in the industry’s potential for job creation and prosperity.  

We greatly appreciate your work in addressing our concerns with the definition of “_broker_” and voice our strong support for passage of this amendment. We agree with the joint statement issued by the Blockchain Association, Coinbase, Coin Center, Ribbit Capital and Square, expressing concern regarding the digital asset provision in the Infrastructure Bill.

Given your keen attention to this matter, we would like to raise our concerns with the current definition of “_digital asset_”. It is our understanding that this provision extends the IRS reporting requirements to cover digital assets. We support the proposed reporting of sales and purchases of cryptocurrencies for investment purposes along with gross proceeds to the IRS, similarly to how the selling of securities is automatically reported to the IRS today. However, the reporting of payment transactions that utilize digital assets would seem to be beyond the initial objective of the Senate. Thus, the “_digital assets_” definition is overly broad and could potentially capture digital dollars and other innovations that are not related to cryptocurrency.  

Currently, in the draft legislative text of the Infrastructure Bill, a “_broker_” will be required to report transfers of “_a covered security which is a digital asset_”.\[1\] Cryptocurrencies purchased for investment purposes would be most likely subject to this reporting requirement. But digital assets used for the purposes of payments such as digital dollars and even Fed-issued central bank digital currencies (CBDC) could be captured as well.  

Unlike the sale and purchase of securities, payment transactions are not investment contracts but rather are the fulfillment of payment obligations. As an example, if PayPal were to use a digital dollar payment platform and transfers of its digital dollars were subject to this IRS reporting requirement, then PayPal would need to report over 15 billion customer transactions to the IRS annually.\[2\] Similarly, users of our digital dollar product may have to have their payment transactions reported to the IRS. And when Fed-issued CBDC is introduced, will payment transactions made in Fed CBDC have to be reported to the IRS? The US financial payment rails may one day run on distributed ledger technologies. When that happens, all payment transactions may very well have to be reported to the US government.  

Against this backdrop, we respectfully ask if it is the objective of the Infrastructure Bill for Americans to report their private payment transactions to the IRS? We question what the IRS would do with this overwhelming amount of data, which often includes sensitive personally identifiable information. Finally, reporting requirements of payment transactions utilizing digital dollars would stifle US private sector innovation in the faster payments sector, leading to continued stagnation in our slow and costly payments system that needs multiple business days to settle a cash transaction.  

We appreciate your time and efforts to bring forth the bipartisan infrastructure language under consideration and sincerely hope that we can work together to clarify this definitional issue.  

Sincerely,  
Linda Jeng and Patrick Murck