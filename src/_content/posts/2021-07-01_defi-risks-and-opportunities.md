---
title: DeFi Risks and Opportunities
slug: defi-risks-and-opportunities
status: live
postDate: 2021-07-01T00:00:00-07:00
teaser: >-
  At Transparent we are building a digital dollar network that is bank-agnostic
  and can provide foundational support for new financial applications in
  traditional finance and, potentially, in the emerging DeFi ecosystem. However,
  the risks posed by DeFi are under-studied and under-documented. As our
  financial services continue to be unbundled and re-bundled on decentralized
  platforms, novel risks inherent to DeFi protocols are being introduced and
  traditional financial risks are being reintroduced, creating different
  transmission channels of risk. This is why it’s important for those of us
  working at fintech firms to understand risk in DeFi as we begin building a new
  kind of digital financial system that is more equitable, resilient and
  efficient.
authors:
  - transparent
---

When Bill Coen, the former Secretary General of the Basel Committee on Banking Supervision, asked me to contribute a chapter to a book he was editing, I jumped at the opportunity and enlisted Nic Carter to be my co-author. The book was to be on RegTech and SupTech, but I wanted to write on a little understood topic that is growing in importance: risks in DeFi.

DeFi or “decentralized finance” is growing in leaps and bounds and – I wager – will become a significant part of our financial system in the near future.

DeFi promises to facilitate cheaper and more open access to financial services by reducing the costs and risks of using centralized intermediaries. Ultimately, DeFi also holds the promise of breaking down financial sector silos, promoting innovation and building more vibrant digital economy.

Foundational elements of the DeFi economy, so-called “stablecoins” are starting to attract some support from regulators like Federal Reserve's Vice-Chair Randy Quarles, who acknowledged in his thoughtful speech earlier this week - the strong US tradition of private sector innovation in payments.

Quarles opined that if the Fed’s concerns, such as run risk, are addressed, they “should be saying yes to these products, rather than straining to find ways to say no.”

At Transparent we are building a digital dollar network that is bank-agnostic and can provide foundational support for new financial applications in traditional finance and, potentially, in the emerging DeFi ecosystem.

However, the risks posed by DeFi are under-studied and under-documented. As our financial services continue to be unbundled and re-bundled on decentralized platforms, novel risks inherent to DeFi protocols are being introduced and traditional financial risks are being reintroduced, creating different transmission channels of risk.

This is why it’s important for those of us working at fintech firms to understand risk in DeFi as we begin building a new kind of digital financial system that is more equitable, resilient and efficient.

Many of the drivers behind DeFi risks stem from the decentralized nature of blockchains. Nic and I conceptually bucketed DeFi risks into five categories:  

1.  interconnections with the traditional financial system,
2.  operational risks stemming from underlying blockchains,
3.  smart contract-based vulnerabilities,
4.  other governance and regulatory risks, and
5.  scalability challenges.  
    

Below I highlight some interesting findings from across these categories.

Category 1. **Interconnectedness with banks** – DeFi, which purports to be about reducing central intermediaries and independently automating the delivery of financial services, is in fact highly interconnected with traditional financial institutions.**‍**

**Underlying reserves** backing USD-denominated stablecoins have to be held somewhere, and they are held at US banks. This arrangement gives banks the power to pull their services and liquidity if they decide it’s too risky. Jamie Dimon, CEO, JPMorgan Chase, famously called bitcoin a "fraud." Take the case of Tether when Wells Fargo ran for the exit sign, leaving Tether scrambling for another liquidity provider. More recently, it was Silvergate discontinuing USD deposits and withdrawals for Binance.**‍**

**Concentration risk** - Less well known outside and perhaps also within the crypto community is that nearly all US institutional crypto-trading desks use the same handful of state-chartered community banks. In the past, the crypto industry was either unbanked or underbanked. Regulators viewed “crypto” suspiciously, and most US banks avoided doing business with crypto-trading firms. Crypto firms were left to choose from but a handful of state-chartered, community banks. Now the resulting concentration risk means disruption at a single bank would incapacitate the entire US crypto industry.

Category 2. **Validator and consensus risks** - if economic incentives are incorrectly aligned, validators/miners can collude and manipulate block formation and transaction histories.**‍**

**Miners/validators can also extract value (MEV)** by frontrunning users and selectively reordering transactions. It is somewhat akin to a high frequency trader paying for order flow in order to trade against uninformed or retail order flow.

Category 3. **Smart contract vulnerabilities** – smart contracts are not legal contracts and they automate the execution of actions. Users using faulty smart contracts could risk losing all their tokens.

Smart contracts that rely on oracles can be attacked via these oracles. If the oracle can send a price input that can trigger liquidation by the smart contract. **‍**

**Flash loans** based on smart contracts is a novel concept that allows the attacker to borrow an unlimited amount of liquidity (up to the size of the loan pool) and the loan must be paid back within the same transaction that it is taken out. This allows attackers access to unlimited leverage.

Category 4. **Governance risks** are numerous, but I will highlight key management as a real vulnerability for DeFi protocols. If key holders lose control of their keys, they lose access and effectively ownership, of their tokens.**‍**

**Admin key abuse** - To address this vulnerability, multi-signature key approaches have been developed, placing user deposits in custody of consortiums. However, placing admin keys in multi-sig key arrangements enable discretionary and opaque control of user funds, which could be exploited.

Category 5. **Scalability challenges** – DeFi can only grow as much as its underlying base layer (i.e., Ethereum, Bitcoin, etc.) can support. Most DeFi protocols are built on the Ethereum platform, which has operational limits. Unless significantly new models for public blockchains are developed, the problem of scalability will be an inherent constraint on the growth of DeFi. Presently, a number of DeFi projects are tackling this foundational issue.

To read more about the DeFi risks we’ve identified, you can access our paper here: [https://papers.ssrn.com/sol3/papers.cfm?abstract\_id=3866705](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3866705)  

Linda Jeng

‍Global Head of Policy & Special Counsel  
Transparent Financial Systems