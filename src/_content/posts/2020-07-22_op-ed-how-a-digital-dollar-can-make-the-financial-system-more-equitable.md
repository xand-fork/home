---
title: How a Digital Dollar Can Make the Financial System More Equitable
slug: op-ed-how-a-digital-dollar-can-make-the-financial-system-more-equitable
status: live
postDate: 2020-07-22T00:00:00-07:00
teaser: >-
  Digital dollars can be designed in any number of ways, and these design
  choices will have profound implications for the people who use them. Congress
  should carefully consider why we need a digital dollar and what should be the
  core objectives. Is the goal of a digital dollar simply to make payments more
  efficient, or do we want a digital dollar that helps build a more equitable
  and inclusive financial system?
authors:
  - patrick-murck
---

Under the strain of civil unrest and the COVID-19 lockdown, cracks that have long existed in the American economic system are widening into fissures, from health care and child care to food production and financial services. The accumulation of these fissures is exacerbating an already significant wealth and opportunity gap between richer and poorer American households, one that all too often falls along racial lines. According to Pew Research Center, the U.S. middle class has been shrinking over the past five decades and is no longer a large majority.

The wealth gap between upper-income and middle- and lower-income families is growing apace. Since 1983, the share of U.S. aggregate wealth of upper income families grew from 60% to 79%, while shares of wealth for middle income families fell from 32% to 17%, and from 7% to 4% for lower income families. Nearly half of all Americans live paycheck to paycheck.

In a recent Federal Reserve survey, one in five American workers lost their jobs in March, among which 40% were from low-income households making less than $40,000. The gap in access to financial services has made it difficult for even the government to quickly send rescue money to those in need under the CARES Act.

A few members in Congress had the courage to be innovative and introduced bills proposing a Fed-run digital dollar, but the focus of their bills was limited to a “digital dollar” that was merely a conduit for American households and businesses to open accounts directly with the Federal Reserve. Elsewhere in the private sector, Facebook revised its Libra offering in an attempt to placate the many and varied critiques of its first crypto-currency proposal. Libra’s response was simply falling back on conventional and not particularly innovative approaches.

These discussions are neglecting how technology design often reinforces the status quo and the gaps between the haves and the have-nots of America. Digital dollar efforts need to be intentional about how their designs leverage actual innovation in a way that helps bridge these gaps if our goal is to foster a more equitable and inclusive national economy.

"Community-based ownership would allow the economic gains of the network to be equitably shared and not reserved for those who have access to capital and connections."

Originally [published in CoinDesk](https://www.coindesk.com/how-a-digital-dollar-can-make-the-financial-system-more-equitable) on July 15, 2020