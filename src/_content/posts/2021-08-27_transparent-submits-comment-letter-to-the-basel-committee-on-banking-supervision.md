---
title: >-
  Transparent submits comment letter to the Basel Committee on Banking
  Supervision
slug: >-
  transparent-submits-comment-letter-to-the-basel-committee-on-banking-supervision
status: live
postDate: 2021-08-27T13:02:00-07:00
teaser: >-
  Transparent submits the following comments to the Basel Committee consultation
  on the prudential treatment of cryptoassets.
authors:
  - transparent
---

Our comments focus on the following:  

We agree that it is important to distinguish between digital representations of traditional assets from those that are cryptoassets with stabilization mechanisms. However, “traditional assets” can be a broad group, so it would be helpful to include a category for assets backed solely by fiat currency and used solely for the purposes of making payments. These types of assets should be in their own category in addition to Group 1a and Group 1b.

Tokenized fiat currencies used for payment purposes (for example, digital dollars or digital euros) should be treated as cash-equivalent for legal, regulatory, and accounting purposes. If holding digital dollars required an operational risk add-on or other capital add-ons, then the digital dollars would lose their effectiveness as a medium of exchange.

The recommended prudential treatment for Group 1b and Group2 may create unintended consequences by either preventing banks from participating in the digital financial system or pushing these cryptoassets into a shadow banking environment.

As background, Transparent Financial Systems, Inc.(Transparent)1 is a technology software provider. Transparent is developing aB2B digital dollar payment solution called Xand, which leverages distributed ledger technology (DLT) to make payments more efficient, secure, and equitable.Our digital dollar product Xand is designed for transactional communities of businesses that regularly transact and do business with each other.

While Transparent is currently focused on the US, it was important that we weighed in on this consultation. The digital world is not limited by borders, and to promote innovation, it is important for regulators around the world to coordinate as they consider how best to regulate the cryptoassets industry.

We have developed a DLT-enabled digital dollar payment solution called Xand that gives transactional communities, which are groups of businesses that transact frequently with each other, the tools to establish their own respective payment networks. Through Xand, businesses can use permissioned, decentralized networks that do not require expensive central intermediaries to send value in real-time and settle on-demand.

Xand also facilitates the building and retention of deposits on the balance sheets of Xand-supporting banks, which hold cash reserves backing the digital dollars. We have also designed Xand, so banks can use their own APIs to communicate with Xand networks – no proprietary software required.

We do all this while adhering to financial regulatory requirements here in the US. Because of our role in digital dollar innovation, we have made some observations with a direct view of how businesses and banks can use digital dollars.

‍**Taxonomy of Digital Assets**

Before we address some of the questions in the consultation, it is important to take the opportunity to discuss the taxonomy of cryptoassets, as we see them in the industry. We have observed the emergence of many different terms that could fall under the nomenclature of “digital assets,” including “digital dollars,” “stablecoins,” “cryptocurrencies,”“cryptoassets,” “central bank digital currencies,” and “tokens,” to name a few.

Many of these new terms do not have official definitions, and some terms are simply incorrect or misleading. For example, many so-called “stablecoins” do not even have stable prices. However, the term “stablecoin”,for example, potentially covers many different types of instruments. Some stablecoins are fully backed by reserves. Others only partially backed. A few, such as algorithmic stablecoins, are not backed by reserves at all. There serves, themselves, can be any combination of fiat currencies, treasuries, commercial paper, corporate bonds, cryptocurrencies, or other types of assets.Some of these stablecoins could very well be securities under US securities laws.

Conversely, compared to stablecoins, “digital dollars” are digital representations of the US dollar. They are fully backed by US dollars and do not require centralized price stabilization management or control.

Drawing a clear distinction within the digital assets taxonomy is necessary for legal and regulatory clarity and is essential for the healthy growth of digital dollar-based payment systems. Absolute legal and regulatory clarity for digital dollars is important for a number of reasons, some of which are described below. The reasons below are US-specific, but the principles are probably universal:

1\. **Private commercial law**

In the United States, the Uniform Commercial Code (UCC)harmonized state laws in the United States beginning in the 1890s and views assets in essentially three categories: money, tangible assets and intangible assets. Tangible assets include land, equipment, inventory, etc., and are physical in nature as opposed to intangible assets, which are not physical in nature but have potential monetary value, such as intellectual property and goodwill.Tangible assets also include financial assets, such as stocks, bonds and cash, which are categorized as current assets because they can be readily sold for cash.

As a type of digital asset, stablecoins that are securities would be subject to Article 8 of the UCC, which governs how securities are held and transferred and how ownership and other interests in securities can be recorded and changed. Interestingly, loans secured by stablecoins would be subject to Article 9, which governs secured transactions. This means a senior creditor’s lien on a stablecoin would travel with such a stablecoin to the new owner unless the stablecoin were a security and the new owner acquired the security from a securities intermediary, which is usually an SEC-registered clearing corporation or broker-dealer.

Money, on the other hand, is treated quite differently under the UCC, which defines “money” as “a medium of exchange currently authorized or adopted by a domestic or foreign government as a part of its currency.” Thus, money is usually regarded as tangible physical cash and not as rights against a depository bank represented by funds in a deposit account. A creditor can perfect a security interest in cash posted as collateral only if the creditor possesses the cash or controls the deposit account holding the cash. If the creditor does not possess the cash or control the deposit account, it loses its perfected interest in the cash collateral.

In other words, a creditor’s lien cannot travel with money.If you offer money to pay for something, the seller must accept it as a means of payment and not worry if a third party might have a claim to that money.This framework allows money to be used as a medium of exchange that is fungible, durable, recognizable, and divisible.

2\. **Financial regulation**

Defining whether a digital dollar is cash-equivalent, or another type of asset could also affect the depository institutions’ ability to hold digital dollars on their balance sheets or even to hold them as custodians. If digital dollars are not considered to be cash-equivalent, then banks would be required to hold capital against digital dollars. We agree with the statement in your Consultative Document that such “cryptoassets must confer the same level of legal rights as cash held in custody.”

But if digital dollars are deemed to be securities under US securities laws, then they would be subject to securities disclosure, prospectus, listing and trading rules, making digital dollars as securities an inefficient medium of ready exchange.

3\. **Accounting**

For similar reasons as those described above, if digital dollars are not considered to be cash-equivalent than there could be double counting of the dollar reserves and their digital representations.

Accordingly, if we want virtual currencies, such as the digital dollar, to work as a medium of exchange and support innovative payment solutions, we need to ensure our laws and regulations clearly recognize digital dollars as money. Otherwise, digital dollars cannot be fungible, divisible, and transferable like money. In addition, the knock-on effects in accounting treatment, bank capital requirements, securities regulation, etc., would destroy the incentives in ever using digital dollars as an efficient means of payment. Thus, regulators need to take great care when developing their taxonomies of the digital assets world and correctly distinguish digital dollars from stablecoins and other digital assets because a digital dollar is still a dollar.

‍**Responses to BCBS Consultation Questions**

Given the viewpoints above, we have focused our comments on the highlighted questions from the consultation.

Q1. What are your views on the Committee’s general principles?

We appreciate how defining cryptoassets so that they fit into one of the various categories is a challenging and important task. We recommend that the Committee continually evaluate these categories in addition to adding additional categories as the cryptoasset industry evolves, including the one category described below (as well as above).

We do not work with Groups 1b and 2 assets, but we wish to opine on the proposed risk weighting. We are concerned that such a conservative approach will create unintended consequences and limit innovation. Banks, for example, may limit their services for any asset utilizing DLT. This could lead to non banks filling the void by increasing their DLT offerings. Another unintended consequence of a conservative approach could be that these activities may migrate from banks to less regulated, shadow banking space. More important, regulators would lose critical visibility into the cryptoasset market.

As such, we would recommend that the Committee evaluate the level of prudential regulation offered across all Groups of cryptoassets.

Q2. What are your views on the Committee’s approach to classify cryptoassets through a set of classification conditions? Do you think these conditions and the resulting categories of cryptoassets (Group 1a, 1b, and 2) are appropriate? Which existing cryptoassets would likely meet the Group1 classification conditions?

We appreciate the Committee’s classification of cryptoassets as a way to clarify what fits into the prudential regulation. However, this classification is not as straightforward as one may think. One category missed in this classification is the DLT-based digital money, such as the “digital dollar,” “digital euro,” “digital yen,” etc.

Drawing a clear distinction within the digital assets taxonomy is necessary for legal and regulatory clarity and is essential for the healthy growth of innovation around cryptoassets.

Assets backed solely by fiat currency and used solely for the purposes of making payments should be in their own group. This group of cash-equivalent instruments need to be treated like the cash-equivalent instruments they are. There should be no need for a capital charge nor for an operational risk add-on.

By including digital dollars in their own category of cryptoassets, banks will be able to differentiate these from cryptoassets on their balance sheets, use them as cash instruments for payment purposes, provide faster payment options for its customers, or even hold digital dollars as custodians.

Linda Jeng

‍Global Head of Policy & Special Counsel  
Transparent Financial Systems