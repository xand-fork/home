---
title: Why We Built the Xand Network Using Substrate
slug: why-we-built-the-xand-network-using-substrate
postDate: 2021-12-10T09:27:00-08:00
status: live
authors:
  - seth-paulson
teaser: Transparent’s payments platform, Xand, is built using Substrate, a
  blockchain framework produced by Parity.
---
### **Why We Built the Xand Network Using Substrate**

In our [previous post](https://transparent.us/post/xand-why-blockchain) we explained what brought us to blockchain technology. In this post, we discuss different options we assessed for how we could use a blockchain with Xand:

1. Develop our functionality on an existing blockchain
2. Write our own blockchain from scratch
3. Fork an existing blockchain codebase and customize it
4. Use a blockchain framework

Like any technical choice, there are pros and cons with each option that we will share below. We ultimately made the decision to build Xand using Substrate - primarily because it allowed us to meet all our requirements for Xand without the cost of building everything ourselves.

#### Option 1: Develop our functionality on an existing blockchain

Building on top of an existing blockchain comes with many advantages. As an extant blockchain would already be fully functional, the only bits to worry about would be implementing our custom logic. An existing network can also be very reliable and offer good security guarantees due to large capitalization and many miners.

First we had to determine if implementing Xand was even possible on existing blockchains. Most existing blockchains offer varying levels of built-in customizability. Bitcoin has Bitcoin Script, which allows for some basic programmability in transactions and more programmability is possible via sidechains. Ethereum has Solidity, which allows for fully programmable “Smart Contracts”: many projects are built on top of Ethereum. Given the complexity of some projects on Ethereum and Bitcoin, it was clear that it would be possible to implement Xand on either or a blockchain with similar capabilities.

Unfortunately, even though it was possible to customize Ethereum, or other chains with similar capabilities, we discovered numerous disadvantages for what we wanted to accomplish with Xand.

Finality was a significant issue with many blockchains. With Proof of Work, finality is probabilistic: Bitcoin has seen great success using Proof of Work, but we have seen attacks on Ethereum Classic (a fork of Ethereum) that reversed transactions that were believed to have been final for many hours. Having a clear definition of finality was a critical requirement for our network and because of this, probabilistic finality was a deal breaker.

However, the most meaningful limit for us was scaling. Bitcoin processes about 7 transactions per second while Ethereum is closer to 14. Worse, these slots are shared among all participants and awarded via a bidding mechanism, meaning users must pay for each transaction. Currently for example, the Bitcoin transaction fee is over $2 while Ethereum averages over $4. Both have been over $50 in the past. High fees contradicted one of Xand's key design principles to minimize or eliminate network transaction fees. Because of these downsides, development on an existing chain was not considered a viable option.

#### Option 2: Write our own blockchain from scratch

Many of the new and innovative ideas in the space involve writing everything needed for a blockchain from the ground up. This would allow us maximum flexibility in how to use blockchain. However, this option would also be extremely expensive. Blockchains require extensive distributed computing and cryptographic work and these are spaces well known to be hard problems where even tiny bugs can result in catastrophic vulnerabilities. As we only needed to use blockchain as a ledger in our product, writing our own would be akin to writing our own database. We did investigate writing our own blockchain at one point. However, we ultimately decided that the expense was not worth it as our need for a blockchain could be satisfied by other options.

#### Option 3: Fork an existing blockchain codebase and customize it

Since most blockchains are open source, one option was to fork an existing blockchain and customize it, as ConsenSys [Quorum](https://consensys.net/quorum/) did with Ethereum. At first this seems like an easier path than Option 2. Over time though, maintaining and upgrading a forked version results in similar challenges to Option 2. We would need to keep merging in updates from the parent as security issues and bugs were found and patched, or else hire the expertise and resources on hand to do it ourselves. Depending on how much our custom version had changed from the original this could be painful and expensive. There was also risk when modifying an unfamiliar codebase, as there can be unclear assumptions that lead to errors. We investigated this as an option for Xand, but because existing codebases were not designed for the kind of flexibility we needed, we ultimately decided that this option would also be too expensive.

#### Option 4: Use A Blockchain Framework

A blockchain framework is a kind of customizable blockchain that provides the basic functionality of a blockchain with hooks to build custom logic. These offerings come ‘batteries included’ with peering, consensus, blockchain storage, and other important systems built–in, while allowing you to plug in your own custom transactions, verification rules, and state. A blockchain framework allows for extensive customizability while allowing a team to get off the ground quickly. As an example, the [Hyperledger Foundation](https://www.hyperledger.org/use/distributed-ledgers) offers a number of open-source solutions. By using a framework, we would not be taking on the burden of writing and maintaining all code for a blockchain node and could benefit from contributions from the community. Our original demos were built using Hyperledger Sawtooth. However, we ran into issues with building a custom consensus system in Sawtooth and repeatedly had issues upgrading because the Hyperledger team was rewriting Sawtooth from Python into Rust.

### How We Chose Substrate

After ruling out Option 1, our initial implementation used the Hyperledger Sawtooth blockchain framework. Because of the issues we experienced with Sawtooth, we searched for a replacement. We considered forking several projects (Option 3) and even began prototyping a custom-built blockchain (Option 2). Around this time, [Parity released the 1.0 version](https://www.parity.io/blog/substrate-has-arrived/) of their blockchain framework Substrate, so we decided to investigate.

Substrate is written entirely in Rust and uses Rust to write custom transaction logic - this was a great fit for us. We’re primarily a Rust shop because Rust offers a great combination of high speed with security and correctness. Substrate also has a consensus algorithm that was usable with minimal tweaking. Finally, Substrate uniquely allows for forkless automatic upgrades by posting the new logic needed to validate to the blockchain as code. These features combined to make Substrate just right for what we were doing, so we switched over.

Our journey began with us using a framework that wasn’t meeting our needs and though we investigated other options, we ultimately came back to another framework. We chose to build Xand with Substrate because our customers can capture blockchain technology's benefits without building a completely new blockchain from scratch.

If your business struggles with the expense or delay involved with payments, Xand can help! Find out more at <a href="https://transparent.us/how-xand-works">How Xand Works</a>, or read our <a href="https://transparent.us/assets/documents/Xand%20Technical%20White%20Paper%20-%202022-03-23.pdf">technical white paper</a>.

If you’re interested in building something that needs a blockchain, you can learn more about Substrate at <https://substrate.io/technology/>