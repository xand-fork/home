---
title: Feedback on Clarifying Laws Around Cryptocurrency and Blockchain Technologies
slug: feedback-on-clarifying-laws-around-cryptocurrency-and-blockchain-technologies
status: live
postDate: 2021-09-27T00:00:00-07:00
teaser: >-
  Feedback for Senator Toomey and US Senate Banking Committee on Clarifying Laws
  Around Cryptocurrency and Blockchain Technologies.
authors:
  - transparent
---

Senator Toomey  
US Senate Banking Committee  
455 Dirksen Senate  
Office Building  
Washington, D.C 20510  
submissions@banking.senate.gov

Subject: Requests Feedback on Clarifying Laws Around Cryptocurrency and Blockchain Technologies

Date: September 27, 2021

‍

Dear Senator Toomey and Committee staff:

Thank you for the opportunity to provide feedback on cryptocurrency and blockchain technologies. As background, Transparent Financial Systems,Inc. (Transparent) is a technology software provider based in Seattle and founded by the late Paul Allen of Microsoft. Transparent is developing a B2B digital dollar payment solution called Xand, which leverages distributed ledger technology (DLT) to make payments more efficient, secure, and equitable. Our digital dollar product Xand is designed for transactional communities of businesses that regularly transact and do business with each other.

As US businesses find all too familiar, cash payments generally take at least one to five business days to settle in the US. The lengthy settlement times result in costly delays and inefficiencies for US businesses. A few faster payment systems do exist,but they require high entry costs that price out many smaller insured depository institutions(IDIs).

Against this backdrop, we have developed a DLT-enabled digital dollar payment solution called Xand that gives transactional communities, which are groups of businesses that transact frequently with each other, the tools to establish their own respective payment networks. Through Xand, businesses can use permissioned, decentralized networks that do not require expensive central intermediaries to send value in real-time and settle on-demand.

Xand also facilitates the building and retention of deposits on the balance sheets of Xand-enabled banks, which hold cash reserves backing the digital dollars.We have also designed Xand, so banks can use their own APIs to communicate with Xand networks – no proprietary software required.

We do all this while adhering to financial regulatory requirements. Because of our role in digital dollar innovation, we have made some observations with adirect view of how businesses and IDIs can use digital dollars.‍

**I. Need for a Taxonomy of Digital Assets‍**

Before we address some of the questions in the RFI, it is important to take the opportunity to discuss the taxonomy of digital assets, as we see them in the industry. We have observed the emergence of many different terms that could fall under the nomenclature of “digital assets,” including “digital dollars”, “stablecoins”,“cryptocurrencies”, “cryptoassets”, “central bank digital currencies”, and “tokens”, to name a few.

Many of these new terms do not have official definitions,and some terms are simply incorrect or misleading. For example, many so-called “stablecoins” do not even have stable prices. However, the term “stablecoin”, for example, potentially covers many different types of instruments. Some stablecoins are fully backed by reserves. Others only partially backed. A few, such as algorithmic stablecoins, are not backed by reserves at all.The reserves, themselves, can be any combination of fiat currencies, treasuries,cryptocurrencies, or other types of assets. Some of these stablecoins could very well be securities under US securities laws.

Conversely, compared to stablecoins, “digital dollars” are digital representations of the US dollar. They are fully backed by US dollars and do not require centralized price stabilization management or control.

Drawing a clear distinction within the digital asset taxonomy is necessary for legal and regulatory clarity and is essential for the healthy growth of digital dollar-based payment systems. Absolute legal and regulatory clarity for digital dollars is important for a number of reasons, including:  
‍

1\. Private commercial law

The Uniform Commercial Code (UCC) harmonized state laws beginning in the 1890s and views assets in essentially three categories: money, tangible assets and intangible assets. Tangible assets include land, equipment, inventory, etc., and are physical in nature as opposed to intangible assets, which are not physical in nature but have potential monetary value, such as intellectual property and goodwill. Tangible assets also include financial assets, such as stocks, bonds and cash, which are categorized as current assets because they can be readily sold for cash.

As a type of digital asset, stablecoins that are securities would be subject to Article 8 of the UCC, which governs how securities are held and transferred and how ownership and other interests in securities can be recorded and changed. Interestingly, loans secured by stablecoins would be subject to Article 9, which governs secured transactions.This means a senior creditor’s lien on a stablecoin would travel with such a stablecoin to the new owner unless the stablecoin were a security and the new owner acquired the security from a securities intermediary, which is usually an SEC-registered clearing corporation or broker-dealer.

Money, on the other hand, is treated quite differently under the UCC, which defines “money” as “a medium of exchange currently authorized or adopted by a domestic or foreign government as a part of its currency.” Thus, money is usually regarded as tangible physical cash and not as rights against a depository bank represented by funds in a deposit account. A creditor can perfect a security interest in cash posted as collateral only if the creditor possesses the cash or controls the deposit account holding the cash. If the creditor does not possess the cash or control the deposit account, it loses its perfected interest in the cash collateral.

In other words, a creditor’s lien cannot travel with money.If you offer money to pay for something, the seller must accept it as a means of payment and not worry if a third party might have a claim to that money. This framework allows money to be used as a medium of exchange that is fungible, durable, recognizable and divisible.

2\. Financial regulation

Defining whether a digital dollar is cash equivalent or another type of asset could also affect the IDI’s ability to hold digital dollars on their balance sheets or hold them even as custodians. If digital dollars are not considered to be cash equivalent,then banks would be required to hold capital against digital dollars. The Basel Committee’s Consultative Document on the prudential treatment of cryptoasset exposures states that such “cryptoassets must confer the same level of legal rights as cash held in custody.”

But if digital dollars are deemed to be securities under US securities laws, then they would be subject to securities disclosure, prospectus, listing and trading rules, making digital dollars useless as a medium of ready exchange.‍

3\. Accounting

For similar reasons as those described above, if digital dollars are not considered to be cash equivalent than there could be double counting of the dollar reserves and their digital representations.

Accordingly, if we want virtual currencies, such as the digital dollar, to work as a medium of exchange and support innovative payment solutions, we need to ensure our laws and regulations clearly recognize digital dollars as money. Otherwise,digital dollars cannot be fungible, divisible, and transferable like money. In addition, the knock-on effects in accounting treatment, bank capital requirements, securities regulation, etc.,would destroy the incentives in ever using digital dollars as an efficient means of payment. Thus,regulators need to take great care when developing their taxonomies of the digital assets world and correctly distinguish digital dollars from stablecoins and other digital assets because a digital dollar is still a dollar.

**II. Brief description of the proposal and how it will encourage the growth of cryptocurrency and blockchain technology in the United States‍**

If we are to foster growth and innovation in faster payments, we need a clear taxonomy of the different types of digital assets that exist today. The taxonomy should be flexible and able to adapt to a quickly-evolving industry. Yet, the taxonomy should also provide legal certainty.

As examples, different authorities are developing taxonomies now. Currently, the European Union is trying to provide legal and regulatory certainty for blockchain-based applications. Last year, it proposed the Markets in Crypto-Assets Regulation(MiCA), which seeks to provide legal certainty that will encourage market growth and innovation. MiCA proposes four categories:

• Crypto-assets generally, as a “catch-all” category

• Utility Token

• ART – Asset-Referenced Token

• EMT – E-Money Token

The last category: EMT (or E-Money Tokens), reflects the European Commission’s recognition of blockchain-enabled digital euro payment solutions. The US should have a similar dichotomy for digital dollar instruments used for payment purposes only and backed solely by US dollars.

The Basel Committee on Banking Supervision has issued a proposal on the prudential treatment of cryptoassets exposure earlier this summer. The Basel Committee proposed 3 categories of cryptoassets:

• Group 1a cryptoassets: tokenized traditional assets

• Group 1b cryptoassets: cryptoassets with stabilization mechanisms

• Group 2 cryptoassets: all other cryptoassets

In this proposed Basel taxonomy, digital dollar tokens used for payment purposes and backed solely by US dollars would fall into Group 1a and, thus,would not be subject to a capital charge. However, the Basel Committee asked if there should be a capital charge for operational risk. In our comment letter, we argued that digital representations of fiat currencies should be placed in an altogether separate category. Money occupies a special place in monetary, regulatory and private commercial law as described above. Presently, cash is challenging to collect and transport from local branch offices, requiring security guards and armored cars. There are even stories of armored truck companies losing track of millions of dollars for the largest banks and even a Federal Reserve bank. Despite this well-known operational risk, banks are not subject to a capital charge for holding cash. This is because of the special position cash occupies in our legal and monetary system. Digital dollars must be treated as cash-equivalent if we are to have a vibrant and robust faster payments infrastructure that can in turn support and meet the needs of US businesses in a growing economy.

Whereas, in the US, the first time “digital assets” will be defined in federal law will probably be the Infrastructure Bill. The draft digital assets provision covers any digital representation of value using distributed ledger technology. This overly-broad definition would potentially require any payment using digital dollars to be reported to the IRS.Therefore, if the Fed introduced central bank digital currency (CBDC), for example, then billions of CBDC transactions would have to be reported to the IRS, which would have to store and manage all this payments data.  
  
**III. Proposed legislative language**

We suggest two approaches. The first approach is to define the term digital dollars and deem them to be cash equivalent. The second approach is for an interagency working group to conduct a study and develop a classification of digital assets.

**1st approach:**

SEC. XX. DEFINITIONS OF DIGITAL ASSETS

(a) DIGITAL DOLLAR.— ‘‘(A) IN GENERAL. the term ‘digital dollar” means “any digital representation of value backed by US dollars held in deposit accounts at depository institutions which is recorded on a cryptographically secured distributed ledger or any similar technology as specified by the Secretary of the Treasury and used solely for payment purposes. Digital dollars are cash-equivalent to US dollars.

**2nd approach:**

SEC. XX. STUDY ON CLASSIFICATION OF DIGITAL ASSETS

(a) INTERAGENCY WORKING GROUP.—There is established to carryout this section an interagency working group (referred to in this section as the ‘‘interagency group’’) composed of the following members or designees:

(1) The Secretary of the Treasury (referred to in this section as the ‘‘Secretary’’), who shall serve as Chair of the interagency group,

(2) The Chair of the Securities and Exchange Commission,

(3) The Chair of the Commodity Futures Trading Commission

(4) The Chair of the Board of Governors of the Federal Reserve System,

(5) The Chair of the Federal Deposit Insurance Corporation,

(6) The Comptroller of the Office of the Comptroller of the Currency.

(b) ADMINISTRATIVE SUPPORT.—The Treasury shall provide the interagency group such administrative support services as are necessary to enable the interagency group to carry out the functions of the interagency group under this section.

(c) CONSULTATION.—In carrying out this section, the inter-agency group shall consult with representatives of the digital assets industry, including software developers, payments companies, financial technology firms, software developers,payments firms, major cryptomarket participants, consumers, and the general public, as the interagency group determines to be appropriate.

(d) STUDY.—The interagency group shall conduct a study of digital assets market and develop a classification that can contribute to the broader approach to oversight of existing and prospective digital assets markets to ensure an efficient,secure, and transparent digital assets market, including oversight of stablecoins.

(e) REPORT.—Not later than 180 days after the date of enactment of this Act, the interagency group shall submit to Congress a report on the results ofthe study conducted under subsection(b), including recommendations for the oversight of existing and prospective digital assets markets to ensure an efficient, secure, and trans-parent digital assets market, including oversight of stablecoins.

**IV. Other background material as appropriate**

The US has had a long history of private-public partnerships in payments. Nearly every payments option in our country has had significant private sector contribution. We need to continue building on this longstanding tradition that has served our economy well. Take ACHNetworks, for example, which are now run by the Federal Reserve and The Clearing House. It was a group of California bankers concerned by the growing volume of paper checks that formed the Special Committee on Paperless Entries, or SCOPE in 1968along with an American Bankers Association-sponsored study that led to the formation of the first ACH association in California to handle electronic payments in 1972. Today, 94% of Americans are paid in Direct Deposit via ACH.

We hope Congress will continue supporting our nation’s longstanding and beneficial tradition of private-public collaboration in improving our nation’s payment systems. We believe the private sector and blockchain technology have much to contribute to real-time payments innovation and providing faster, cheaper and more secure payments for Americans.We hope our suggestions are helpful, and we are available to discuss any of the above points in more detail.  
  
‍Sincerely,  
Linda Jeng

‍Global Head of Policy & Special Counsel  
Transparent Financial Systems