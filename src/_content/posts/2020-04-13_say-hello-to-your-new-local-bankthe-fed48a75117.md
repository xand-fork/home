---
title: Say Hello to Your New Local Bank – The Fed
slug: say-hello-to-your-new-local-bankthe-fed48a75117
status: live
postDate: 2020-04-13T00:00:00-07:00
teaser: >-
  Several bills were recently introduced on the Hill creating a U.S. “Digital
  Dollar” as part of the coronavirus stimulus package. Transparent Systems
  executives say the aspiration for ubiquitous, cheap access to money is
  admirable but these bills would radically upend the banking and payments
  system we rely on today.
authors:
  - patrick-murck
---

In the recently passed Covid-19 stimulus package, Congress ran into a problem all too familiar to many Americans: it couldn’t easily send people money.

This isn’t a new problem, but the Covid-19 crisis has brought renewed attention to the slow and fragmented nature of our financial and payments infrastructure.

The solution to Congress’ problem, a problem that many of us share, does not have to disrupt the fundamental nature of our modern financial system. Rather we can leverage and enhance public sector oversight to unlock further innovation in the private sector and get our money moving again for the people who need it.

Unless you had closely followed the different versions of the coronavirus economic stimulus package being debated by Congress, you probably weren’t aware of a number of bills proposing a U.S. “Digital Dollar” that had been introduced during the stimulus negotiations.

**Sen. Brown’s Bill**

The most recent of these bills was one introduced by ranking member of the Senate Banking Committee, Sen. Sherrod Brown (D-Ohio) (as of March 24, the final version of the stimulus package offered by the House Democrats quietly dropped the U.S. Digital Dollar proposal, but a bill introduced on March 23 by Chairwoman Maxine Waters (D-Calif.) of the House Financial Services Committee contains related language.).

In his press release announcing his new bill, Brown stated: “My legislation would allow every American to set up a free bank account so they don’t have to rely on expensive check cashers to access their hard-earned money.” His aspiration for ubiquitous, cheap access to money is clear and admirable, but this cure has the nasty side effect of also radically upending the banking and payments system we rely on today.

In an attempt to develop a direct distribution network of stimulus funds to all U.S. residents and citizens, Brown proposed a new framework for a Fed-maintained digital dollar. According to the Brown’s bill, the term “digital dollars” means “dollar balances consisting of digital ledger entries recorded as liabilities in the accounts of any Federal reserve bank.”

Brown’s bill would require commercial banks to establish special purpose vehicles to open digital dollar accounts for consumers (called “pass-through digital wallets”). The funds would ultimately sit and collect interest at a Federal Reserve Bank in digital dollar wallets for which the commercial bank could not charge fees.

Historically, central banks controlled monetary policy through commercial banks which accepted deposits from customers and, in turn, made loans to borrowers while holding a reserve equal to a fraction of their deposit liabilities at the central bank. However, under Brown’s bill, banks would be mandated to find customers for the Fed but could not accept these digital dollar deposits as part of their balance sheet and, thus, cannot leverage these deposits to issue loans.  

**‘Fed as Local Bank’ Model**

In this “Fed as local bank” model, one would have to wonder why even have banks as middle men. But the bigger issue is the unintended (or perhaps intended) ripple effects from adopting a retail central bank digital dollar. Who wouldn’t want to have an account with the safest bank in the country—the Fed? We would no longer need commercial banks. We also would no longer need payment processors, such as Visa, Mastercard, ACH, and perhaps even Fedwire, because one could transfer money directly between Fed digital wallets.

Money and payments in the U.S. has traditionally been a public-private collaboration dating back to the founding of the country, a collaboration that has been reified and formalized over the years from the establishment of the Treasury to the passage of the Federal Reserve Act at the turn of the previous century through to the current FinTech boom in Silicon Valley and beyond. Congress shouldn’t be in a rush now to reinvent the entire U.S. payments system and upend this long standing dynamic. We can and should take a measured and thoughtful approach that preserves the time-worn balance between private sector innovation and public sector oversight.

The bill further requires the U.S. Postal Service to host ATMs where there is no easily accessible branch of a Federal Reserve Bank. While much of this proposal is problematic, it is smart and laudable to leverage existing public sector resources to provide access to banking and payments services and meet demand from under-served communities where the private sector and the market has failed.

But other aspects of the bill seem less thought through. The Fed could participate easily enough in distributing economic stimulus grants via digital wallets. But once these digital dollar accounts are opened at the Fed and people deposit more money, why would anyone continue to make deposits with their local banks?

Present day bank deposits, unlike Fed digital dollars, would be reflected on your local banks balance sheet and loaned back within the community. Once deposits are choked off from local banks, will the Fed be tasked with stepping in to review loan applications from the many hundreds of thousands of local businesses and the millions of people who are being laid off to determine who should get loans?  

**Anti-Money Laundering Compliance Unclear**

It is also unclear how compliance with anti-money laundering statutes and the Bank Secrecy Act would be integrated into the Fed digital dollar program. Banks must report all suspicious activities and transactions; would the Fed now take on this responsibility? Monitoring payment transfers is one of the most important elements of financial surveillance and the prevention of illicit funds flows.

It is inevitable that money would be laundered from a Fed digital dollar account and that money in the program would be used for terrorist financing and sanctions violations. Who should be accountable for reporting these transactions—the Fed, the commercial bank, or postal service, or the bank’s special purpose vehicle? Could the U.S. Attorney’s Office prosecute the Fed for not reporting drug or terrorist payments or transactions violating OFAC sanctions conducted from a Fed digital dollar wallet?

Central banks around the world have been studying and debating how best to design central bank digital currencies, including whether the central bank digital currency should be issued as retail or wholesale digital currencies. The debate has hastened, particularly after Facebook announced its controversial Libra project. However, central banks know that they are not equipped to act as commercial agents if they issued retail digital currency directly to consumers.

**Role of Fed Should Be Clearly Defined**

If Congress intends to legislate a central bank digital currency, it needs to first clearly determine the role of the Fed in the financial system and whether it should be a direct issuer of funds and credit to households and businesses or if banks should keep the function of credit intermediation.

Congress would also need to determine a practical unit of account that appropriately represents the value of the U.S. dollar that ensures the integrity of the U.S. dollar and its preeminent role in the global economy. It should also consider how a central bank digital currency can promote faster payments and cheaper financial services.

Finally, Congress should consider if it wants to establish a Fed-only network that precludes other potentially innovative private-sector solutions that can quickly and efficiently reach our households and businesses.

Perhaps Congress should consider that we would be better served by allowing an ecosystem where different digital currency solutions could compete anchored by our U.S. dollar and the Federal Reserve System.

Originally [published in Bloomberg Law](https://news.bloomberglaw.com/banking-law/insight-say-hello-to-your-new-local-bank-the-fed?context=search&index=4) on April 13, 2020