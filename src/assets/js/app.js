//import 'core-js/stable';
// const axios = require('axios');
// window.axios = axios;

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function svgTextMultiline(elId, y) {
	/* get the text */
	var element = document.getElementById(elId),
		container = (window.isMobile) ? document.getElementById('titleContainerMob') : document.getElementById('titleContainer'),
		curY = 0,
		testWidth = '';
	
	var width = container.getBoundingClientRect().width -30,//give it some padding
		text = element.innerText || element.textContent;//title tag without any tspan tags
	text = text.trim();

	// console.log('ww', width, window.isMobile);

	/* split the words into array */
	var words = text.split(' '),
		line = '',
		html='',
		lc = 0;//line count

	/* Make a tspan for testing */
	element.innerHTML = '<tspan id="PROCESSING">busy</tspan >';

	var line = '';
	var startY = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	for (var n = 0; n < words.length; n++) {
		var testLine = line + words[n] + ' ';
		var testElem = document.getElementById('PROCESSING');

		/* html holder */
		/*  Add line in testElement */
		testElem.innerHTML = testLine;
		/* Messure textElement */
		var metrics = testElem.getBoundingClientRect();
		var testmet = (isFirefox) ? testElem.getBBox() : testElem.getBoundingClientRect();
		var testWidth = testmet.width;
		// console.log('met', testWidth, width);

		if (testWidth > width -30) {
			curY = (lc === 0) ? startY : y;// firt one starts at 0
			lc++;//increase linecount
			html += '<tspan x="0" dy="' + curY + '">' + line + '</tspan>';
			line = words[n] + ' ';//reset with the word that was to big to start
		} else {
			line = testLine;
		}
	}

	html += '<tspan x="0" dy="' + y + '">' + line + '</tspan>';
	element.innerHTML = html;
	// document.getElementById("PROCESSING").remove();
	
}


// window.isMobile = isMobile();
var viewWidth = document.body.clientWidth;
	window.isMobile = (viewWidth <= 640) ? true : false;

var pristine;
window.onload = function () {

	var form = document.getElementById("contact-form");

	pristine = new Pristine(form);
	// console.log('f', form, pristine);

	// OLD CRAFT CONTACT FORM JS
	// form.addEventListener('submit', function (e) {
	// 	e.preventDefault();
	// 	var isValid = pristine.validate();
	// 	// console.log('v', isValid);
	// 	if (isValid) {
	// 		// form.submit();
	// 		var fd = new FormData();
	// 		fd.append("fname", document.getElementById("fname").value);
	// 		fd.append("lname", document.getElementById("lname").value);
	// 		fd.append("email", document.getElementById("email").value);
	// 		fd.append("message", document.getElementById("message").value);
	// 		fd.append("newsletter", document.getElementById("newsletter").checked);
	// 		// fd.append("CRAFT_CSRF_TOKEN", document.querySelector("[name='CRAFT_CSRF_TOKEN']").value);

	// 		// console.log('fd', fd);
	// 		window.axios({
	// 			method: "post",
	// 			url: form.getAttribute('action'),
	// 			data: fd,
	// 			headers: { "Content-Type": "multipart/form-data" },
	// 		  })
	// 		.then(function (response) {
	// 			  //handle success
	// 			  if (response.status == 200) {
	// 				  	form.classList.toggle('hidden');
	// 				 	var thanks = document.getElementById('formThanks'); 
	// 				  	thanks.classList.toggle('hidden');
	// 					thanks.classList.remove('opacity-0');
	// 					thanks.classList.add('opacity-100');
	// 			  }
	// 			//   console.log('resp', response);
	// 			})
	// 		.catch(function (response) {
	// 			  //handle error
	// 			  console.log('resp error', response);
	// 		});
	// 	}
	// });


};

// GO!
document.addEventListener( 'DOMContentLoaded', function () {
	// console.log('page loaded');

	// instantiate any splide carousels
	if (document.querySelectorAll('.splide-carousel').length > 0) {
		// console.log('carousel found');
		new Splide( '.splide-carousel', {
			type   		: 'loop',
			perPage		: 1,
			perMove		: 1,
			arrows		: false,
			autoWidth	: false,
			focus    	: 'center',
		} ).mount();
	}

	//set up the various blog/press heros and their starting Y
	var sizeSVGText = function() {
		if (document.querySelector('#press-index-wrap')) {
			svgTextMultiline('press-index-wrap', 69);
		}
		if (document.querySelector('#press-index-wrap-mobile')) {
			svgTextMultiline('press-index-wrap-mobile', 34);
		}
		if (document.querySelector('#press-article-wrap')) {
			svgTextMultiline('press-article-wrap', 69);
		}
		if (document.querySelector('#press-article-wrap-mobile')) {
			svgTextMultiline('press-article-wrap-mobile', 34);
		}
	}

	// debounced function called on window resize
	// only fires on change in breakpoints
	var resizeSVGText = debounce(function() {
		var curWidth = document.body.clientWidth;
		if ((viewWidth > 640 && curWidth <= 640) || (viewWidth <= 640 && curWidth > 640)) {//breakpoint switch happened
			window.isMobile = (curWidth <= 640) ? true : false;
			viewWidth = curWidth;
			sizeSVGText();
		}
	}, 50);

	//set up handler for window resize and SVG hero text
	if (document.querySelector('#press-article-wrap') || document.querySelector('#press-article-wrap-mobile') || document.querySelector('#press-index-wrap')) {
		// console.log('has svg text');
		setTimeout(function() { sizeSVGText(); }, 100);
		window.addEventListener('resize', function() {
			resizeSVGText();
		});
	}

	// home hero lottie init for desktop
	if (document.querySelector('#homeHero')) {
		const player = document.querySelector('#homeHero');
        player.load('/assets/lottie/home-desktop.json');
	}

	// home hero lottie init for mobile
	if (document.querySelector('#homeHeroMob')) {
		const playerMob = document.querySelector('#homeHeroMob');
        playerMob.load('/assets/lottie/home-mobile.json');
	}

	// Handle ajax for generic contact forms
	const formContact = document.querySelector('#formContact');
	if (formContact) {
		formContact.addEventListener('submit', event => {
			event.preventDefault();
			const formData = new FormData(formContact);
	
			axios.post('/wheelform/message/send', formData)
				.then(function (response) {
					// console.log('resp', response);
					if (response.data.errors) {// display errors
						let errorsHTML = '',
							errors = Object.values(response.data.errors);
						for(let i = 0; i < errors.length; i++) {
							errorsHTML += '<p class="err">'+ errors[i] +'</p>';
						}
						document.querySelector(".errors").innerHTML = errorsHTML;
					} else {// display thank you
						document.getElementById("formContact").classList.add('hidden');
						document.getElementById("formContactThanks").classList.remove('hidden');
					}
				})
				.catch(function (error) {
					document.getElementById("form-contact").innerHTML = '<div><h3>Sorry, there was an error.</h3><p>The following errors were encountered:</p><p class="text-dcired">'+ error +'</p></div>';
				});
		});
	}
});